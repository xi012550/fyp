package Objects;

public class Items implements java.io.Serializable {
    protected String description; // The description of the Items.
    protected String Name; // The name of the Items.
    public int itemValue;

    // Default constructor for the Items class.
    public Items() {
    }

    // Getter method for the description field.
    public String getDescription() {
        return description;
    }

    // Setter method for the description field.
    public void setDescription(String description) {
        this.description = description;
    }

    // Getter method for the Name field.
    public String getName() {
        return Name;
    }

    // Setter method for the Name field.
    public void setName(String Name) {
        this.Name = Name;
    }

    // Getter method for the itemValue field.
    public int getItemValue() {
        return itemValue;
    }

    // Setter method for the itemValue field.
    public void setItemValue(int itemValue) {
        this.itemValue = itemValue;
    }
}