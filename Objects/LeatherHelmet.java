package Objects;

public class LeatherHelmet extends Helmet implements java.io.Serializable {

    public LeatherHelmet() {
        super("");
        setName("Leather Helmet");
        setDefence(0.02);
    }

}
