package Objects;

public class LeatherChestpiece extends Chest implements java.io.Serializable {

    public LeatherChestpiece() {
        super("");
        setName("Leather Chestpiece");
        setDefence(0.05);
    }

}