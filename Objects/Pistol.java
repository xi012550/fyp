package Objects;

//create a pistol class that works just like machete 

public class Pistol extends Weapon implements java.io.Serializable {

    // Constructor for the Pistol class that takes in the damage value as a
    // parameter.
    public Pistol(int quality) {
        super(0, "");
        Boolean isRanged = true;
        // if the quality is 1, the damage is 5 and the name is "Rusty Pistol", if the
        // quality is 2, the damage is 10 and the name is "Pistol", if the quality is
        // 3, the damage is 15 and the name is "Sharp Pistol"
        if (quality == 1) {
            super.setDamage(5);
            super.setName("Damaged Pistol");
        } else if (quality == 2) {
            super.setDamage(10);
            super.setName("Pistol");
        } else if (quality == 3) {
            super.setDamage(15);
            super.setName("Quality Pistol");
        }

    }
}