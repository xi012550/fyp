package Objects;

public class Machete extends Weapon implements java.io.Serializable {

    // Constructor for the Machete class that takes in the damage value as a
    // parameter.
    public Machete(int quality) {
        super(0, "");
        Boolean isRanged = false;
        // if the quality is 1, the damage is 5 and the name is "Rusty Machete", if the
        // quality is 2, the damage is 10 and the name is "Machete", if the quality is
        // 3, the damage is 15 and the name is "Sharp Machete"
        if (quality == 1) {
            super.setDamage(3);
            super.setName("Rusty Machete");
            super.setItemValue(5);
        } else if (quality == 2) {
            super.setDamage(8);
            super.setName("Machete");
            super.setItemValue(50);
        } else if (quality == 3) {
            super.setDamage(15);
            super.setName("Sharp Machete");
            super.setItemValue(100);
        }

    }
}
