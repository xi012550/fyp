package Objects;

// This class represents a weapon object that has a damage value.
public class Weapon extends Items implements java.io.Serializable {

    private int damage; // The damage value of the weapon.
    private int quality; // The quality of the weapon.
    private String name; // The name of the weapon.
    public Boolean isRanged; // Whether the weapon is ranged or not.

    // Constructor for the Weapon class that takes in the damage value as a
    // parameter.
    public Weapon(int quality, String name) {
        isRanged = false;
        this.quality = quality;
    }

    // Getter method for the damage field.
    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }
}