package Network;

import java.io.*;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;

import Classes.BaseClass.*;
import Combat.CombatLoop;
import Classes.Mechanic;
import Classes.Mercenary;
import Classes.Psion;
import Classes.Scout;
import Entities.NPC;
import Entities.NPCMerchant;
import Entities.Player;
import Objects.Chest;
import Objects.Helmet;
import Objects.Items;
import Objects.Legs;
import Objects.Weapon;
import Other.ClassAbilities;
import Races.Cyborg;
import Races.Human;
import Races.Mutant;
import Races.Starborn;
import World.Map;
import World.Room;

import Other.Commands;

public class ClientHandler implements Runnable {

    // private variables for this class
    private Socket socket; // the socket used to communicate with the client
    private Server mainServer; // a reference to the main server
    public BufferedWriter BW; // used to send data to the client
    public BufferedReader BR; // used to read data from the client
    public Player assignedCharacter = new Player(); // the player assigned to this client
    private int ID; // the client's ID number
    private boolean signedIn; // whether or not the client is signed in

    public ClientHandler(Socket socket, int clientID, Server server) {
        // constructor for the ClientHandler class
        ID = clientID;
        try {
            // set up the client's socket and streams
            this.socket = socket;
            this.mainServer = server;
            this.BW = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            this.BR = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.signedIn = false;
        } catch (IOException e) {
            closeEverything(socket, BR, BW);
        }
    }

    // getters for the player's name, password, and class, and for the client's ID
    public String retrievePlayerName() {
        return this.assignedCharacter.getPlayerName();
    }

    public String retrievePlayerPassword() {
        return this.assignedCharacter.getPlayerPassword();
    }

    public String retrievePlayerClass() {
        return this.assignedCharacter.getPlayerClass();
    }

    public int retrieveID() {
        return this.ID;
    }

    @Override
    public void run() {
        // run method for the ClientHandler class
        String messageFromClient;
        while (this.signedIn == false) {
            logonSystem();
        }

        while (socket.isConnected()) {
            // continuously check for input from the client
            try {
                messageFromClient = BR.readLine();
                System.out.printf(messageFromClient + "\n");
                processPlayerAction(messageFromClient);
            } catch (IOException e) {
                closeEverything(socket, BR, BW);
                break;
            }
        }
    }

    public void processPlayerAction(String playerCommand) {
        // method to handle input from the client
        for (ClientHandler clientHandler : mainServer.clientHandlers) {
            try {
                if (clientHandler.ID == this.ID) {
                    if (playerCommand.equalsIgnoreCase("Look")) {
                        Commands.lookAction(clientHandler, mainServer, assignedCharacter);
                    }
                    if (playerCommand.equalsIgnoreCase("Move")) {
                        // ask the user which direction they want to move in
                        clientHandler.BW.write("What direction would you like to move?");
                        clientHandler.BW.newLine();
                        clientHandler.BW.flush();
                        String direction = BR.readLine();
                        direction = direction.toUpperCase();
                        Commands.moveAction(clientHandler, direction, mainServer, assignedCharacter);
                    }
                    if (playerCommand.equalsIgnoreCase("Take")) {
                        // ask user what item they'd like to take
                        clientHandler.BW.write("Which item would you like to take?");
                        clientHandler.BW.newLine();
                        clientHandler.BW.flush();
                        String itemToBeTaken = BR.readLine();
                        itemToBeTaken = itemToBeTaken.toUpperCase();
                        Commands.takeAction(clientHandler, itemToBeTaken, mainServer, assignedCharacter);
                    }
                    // if the player types "exits" they will be shown the exits in the room
                    if (playerCommand.equalsIgnoreCase("Exits")) {
                        for (Room Room : mainServer.map.worldList) {
                            if (Room.id == assignedCharacter.playerLocationReference) {
                                clientHandler.BW.write(Room.getExits());
                                clientHandler.BW.newLine();
                                clientHandler.BW.flush();
                            }
                        }
                    }
                    if (playerCommand.equalsIgnoreCase("Inventory")) {
                        // showcase user inventory
                        clientHandler.BW.write(assignedCharacter.getInventory());
                        clientHandler.BW.newLine();
                        clientHandler.BW.flush();
                    }
                    if (playerCommand.equalsIgnoreCase("Equip")) {
                        // ask user which item they'd like to equip
                        clientHandler.BW.write("Which item would you like to equip?");
                        clientHandler.BW.newLine();
                        clientHandler.BW.flush();
                        String itemToBeEquipped = BR.readLine();
                        Commands.equipAction(clientHandler, itemToBeEquipped, assignedCharacter);
                    }
                    if (playerCommand.equalsIgnoreCase("Unequip")) {
                        // ask user which item they'd like to unequip
                        clientHandler.BW.write("Which item would you like to unequip?");
                        clientHandler.BW.newLine();
                        clientHandler.BW.flush();
                        String itemToBeUnequipped = BR.readLine();
                        Commands.unequipAction(clientHandler, itemToBeUnequipped, assignedCharacter);
                    }
                    if (playerCommand.equalsIgnoreCase("Self")) {
                        // display user details to self
                        clientHandler.BW.write(assignedCharacter.displayPlayerInformation());
                        clientHandler.BW.newLine();
                        clientHandler.BW.flush();
                    }
                    if (playerCommand.equalsIgnoreCase("Drop")) {
                        // ask user what item they'd like to drop
                        clientHandler.BW.write("Which item would you like to drop?");
                        clientHandler.BW.newLine();
                        clientHandler.BW.flush();
                        String itemToBeDropped = BR.readLine();
                        Commands.dropAction(clientHandler, itemToBeDropped, mainServer, assignedCharacter);
                    }
                    // add experience command
                    if (playerCommand.equalsIgnoreCase("Experience")) {
                        clientHandler.BW.write("How much experience would you like to add?");
                        clientHandler.BW.newLine();
                        clientHandler.BW.flush();
                        String experienceToAdd = BR.readLine();
                        int experienceToAddInt = Integer.parseInt(experienceToAdd);
                        giveExperience(experienceToAddInt);
                    }
                    // add a player command that allows the player to talk to an npc
                    if (playerCommand.equalsIgnoreCase("Talk")) {
                        clientHandler.BW.write("Who would you like to talk to?");
                        clientHandler.BW.newLine();
                        clientHandler.BW.flush();
                        String target = BR.readLine();
                        target = target.toUpperCase();
                        if (target.equals("NPC")) {
                            clientHandler.BW.write("Which NPC?");
                            clientHandler.BW.newLine();
                            clientHandler.BW.flush();
                            String npcToBeTalkedTo = BR.readLine();
                            npcToBeTalkedTo = npcToBeTalkedTo.toUpperCase();
                            Commands.talkAction(clientHandler, npcToBeTalkedTo, mainServer, assignedCharacter);
                        } else if (target.equals("FACTION")) {
                            clientHandler.BW.write("Type your message to your faction: "
                                    + clientHandler.assignedCharacter.getFaction() + "\n");
                            clientHandler.BW.newLine();
                            clientHandler.BW.flush();
                            String messageToSend = BR.readLine();
                            if (clientHandler.assignedCharacter.getFaction() != "None") {
                                for (ClientHandler clients : mainServer.clientHandlers) {
                                    try {
                                        if (clients.ID != this.ID && clients.assignedCharacter
                                                .getFaction() == assignedCharacter.getFaction()) {
                                            clients.BW.write(
                                                    clientHandler.assignedCharacter.getPlayerName() + ": "
                                                            + messageToSend);
                                            clients.BW.newLine();
                                            clients.BW.flush();
                                        }

                                    } catch (IOException e) {
                                        closeEverything(socket, BR, BW);
                                    }
                                }
                            }
                        }
                        // send a message to all players in the same room
                        else if (target.equals("ROOM")) {
                            clientHandler.BW.write("Type your message to the room: " + "\n");
                            clientHandler.BW.newLine();
                            clientHandler.BW.flush();
                            String messageToSend = BR.readLine();
                            for (ClientHandler clients : mainServer.clientHandlers) {
                                try {
                                    if (clients.ID != this.ID
                                            && clients.assignedCharacter.playerLocationReference == assignedCharacter.playerLocationReference) {
                                        clients.BW.write(
                                                clientHandler.assignedCharacter.getPlayerName() + ": " + messageToSend);
                                        clients.BW.newLine();
                                        clients.BW.flush();
                                    }

                                } catch (IOException e) {
                                    closeEverything(socket, BR, BW);
                                }
                            }
                        } else {
                            clientHandler.BW.write("Invalid target");
                            clientHandler.BW.newLine();
                            clientHandler.BW.flush();
                        }

                    }
                    // add an exit command
                    if (playerCommand.equalsIgnoreCase("Quit")) {
                        clientHandler.BW.write("Are you sure you'd like to exit the game? (Y/N)");
                        clientHandler.BW.newLine();
                        clientHandler.BW.flush();
                        String exitConfirmation = BR.readLine();
                        exitConfirmation = exitConfirmation.toUpperCase();
                        if (exitConfirmation.equals("Y")) {
                            for (Room Room : mainServer.map.worldList) {
                                if (Room.id == assignedCharacter.playerLocationReference) {
                                    Room.removePlayer(assignedCharacter);
                                }
                            }
                            closeEverything(socket, BR, BW);
                        }
                    }
                    // add a command that allows the player to create a faction using createfaction
                    // in commands
                    if (playerCommand.equalsIgnoreCase("Create Faction")) {
                        Commands.createFaction(clientHandler);
                    }

                    // add a command that uses the invite faction command in commands
                    if (playerCommand.equalsIgnoreCase("Invite")) {
                        Commands.inviteFaction(clientHandler, mainServer);
                    }

                    if (playerCommand.equalsIgnoreCase("Quest")) {
                        Commands.questAction(clientHandler, assignedCharacter);
                    }
                    if (playerCommand.equalsIgnoreCase("Fight")) {
                        clientHandler.BW.write("Are you sure you'd like to fight the enemies in this room? (Y/N)");
                        clientHandler.BW.newLine();
                        clientHandler.BW.flush();
                        String fightConfirmation = BR.readLine();
                        fightConfirmation = fightConfirmation.toUpperCase();
                        if (fightConfirmation.equals("Y")) {
                            // check if there's an enemy in the room
                            for (Room Room : mainServer.map.worldList) {
                                if (Room.id == assignedCharacter.playerLocationReference) {
                                    if (Room.getEnemies().isEmpty()) {
                                        clientHandler.BW.write("There are no enemies in this room!");
                                        clientHandler.BW.newLine();
                                        clientHandler.BW.flush();
                                        break;
                                    }
                                    // check if a different player is already in combat in the same room

                                    for (ClientHandler clientHandler1 : mainServer.clientHandlers) {
                                        if (clientHandler1.assignedCharacter.inCombat == true
                                                && clientHandler1.assignedCharacter.playerLocationReference == assignedCharacter.playerLocationReference) {
                                            clientHandler.BW.write("There is already a combat occuring here!");
                                            clientHandler.BW.newLine();
                                            clientHandler.BW.flush();
                                            break;
                                        } else {
                                            assignedCharacter.inCombat = true;
                                            if (CombatLoop.start(assignedCharacter, Room.getEnemies(),
                                                    clientHandler1) == true) {
                                                Room.getEnemies().clear();
                                            }
                                            assignedCharacter.inCombat = false;
                                            break;
                                        }
                                    }
                                }

                            }
                        }

                        if (playerCommand.equalsIgnoreCase("Set Spawn")) {
                            Commands.setSpawnPoint(clientHandler, mainServer, assignedCharacter);
                        }

                        if (playerCommand.equalsIgnoreCase("Help")) {
                            Commands.helpAction(clientHandler);
                        }
                    }
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void savePlayerToFile(Player player) throws IOException {
        try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream("players.dat", true))) {
            outputStream.writeObject(player);
        } catch (IOException e) {
            throw new IOException("Failed to save player data", e);
        }
    }

    public String checkLogonDetails(String username, String password) {
        try {
            // Create a FileInputStream for the players.dat file
            FileInputStream fileIn = new FileInputStream("players.dat");

            // Create an ObjectInputStream to read objects from the file stream
            ObjectInputStream objectIn = new ObjectInputStream(fileIn);

            // Loop through the objects in the file and check if any match the given
            // username and password
            Player player;
            while ((player = (Player) objectIn.readObject()) != null) {
                if (player.getPlayerName().equals(username) && player.getPlayerPassword().equals(password)) {
                    // Close the input streams and return the matching player object
                    objectIn.close();
                    fileIn.close();
                    return "Existing Logon";
                }
            }

            // check if only the username is correct
            while ((player = (Player) objectIn.readObject()) != null) {
                if (player.getPlayerName().equals(username)) {
                    // Close the input streams and return the matching player object
                    objectIn.close();
                    fileIn.close();
                    return "Username Taken";
                }
            }

            // If no matching player object is found, close the input streams and return
            // null
            objectIn.close();
            fileIn.close();
            return "Free Logon";

        } catch (EOFException e) {
            // End of file reached, no more objects to read
            return "Free Logon";
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Player login(String username, String password) {
        try {
            // Create a FileInputStream for the players.dat file
            FileInputStream fileIn = new FileInputStream("players.dat");

            // Create an ObjectInputStream to read objects from the file stream
            ObjectInputStream objectIn = new ObjectInputStream(fileIn);

            // Loop through the objects in the file and check if any match the given
            // username and password
            Player player;
            while ((player = (Player) objectIn.readObject()) != null) {
                if (player.getPlayerName().equals(username) && player.getPlayerPassword().equals(password)) {
                    // Close the input streams and return the matching player object
                    objectIn.close();
                    fileIn.close();
                    return player;
                }
            }

            // If no matching player object is found, close the input streams and return
            // null
            objectIn.close();
            fileIn.close();
            return null;

        } catch (EOFException e) {
            // End of file reached, no more objects to read
            return null;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void logonSystem() {
        // detailed logon system that compares existing user details and then either
        // creates a new account, or signs you into your old one
        for (ClientHandler clientHandler : mainServer.clientHandlers) {
            try {
                if (clientHandler.ID == this.ID) {

                    clientHandler.BW.write("Please enter your username");
                    clientHandler.BW.newLine();
                    clientHandler.BW.flush();
                    this.assignedCharacter.setPlayerName(BR.readLine());

                    clientHandler.BW.write("Please enter your password");
                    clientHandler.BW.newLine();
                    clientHandler.BW.flush();
                    this.assignedCharacter.setPlayerPassword(BR.readLine());

                    if (checkLogonDetails(this.assignedCharacter.getPlayerName(),
                            this.assignedCharacter.getPlayerPassword()) == "Free Logon") {
                        // ask the user if they want to create a new account or try again
                        clientHandler.BW.write(
                                "Sorry this account does not exist, would you like to create a new account? (Y/N)"
                                        + "\n");
                        clientHandler.BW.flush();
                        String answer = BR.readLine();
                        // check if answer is yes or no, run the character creation method if its yes,
                        // break otherwise.
                        if (answer.equals("Y") || answer.equals("y")) {
                            characterCreation(clientHandler);
                            this.signedIn = true;
                        } else {
                            break;
                        }

                    } else if (checkAccountActivity(this.assignedCharacter.getPlayerName(),
                            this.assignedCharacter.getPlayerPassword(), this.retrieveID()) == true) {
                        clientHandler.BW.write("Sorry this account is already logged in, please try again." + "\n");
                        break;
                    }
                    if (checkLogonDetails(this.assignedCharacter.getPlayerName(),
                            this.assignedCharacter.getPlayerPassword()) == "Existing Logon") {
                        this.assignedCharacter = login(this.assignedCharacter.getPlayerName(),
                                this.assignedCharacter.getPlayerPassword());
                        clientHandler.BW.write("Welcome back " + this.assignedCharacter.getPlayerName() + "\n");
                        clientHandler.BW.flush();
                        this.signedIn = true;
                    }

                }
            } catch (IOException e) {
                closeEverything(socket, BR, BW);
            }
        }

        for (Room Room : mainServer.map.worldList) {
            if (Room.id == assignedCharacter.getPlayerLocationReference()) {
                Room.addPlayer(assignedCharacter);
            }
        }
        // broadcast a message to all clients when a new user has entered the chat,
        // placeholder
        broadcastMessage(this.assignedCharacter.getPlayerName() + " has entered the world.");

    }

    private void characterCreation(ClientHandler clientHandler) {
        // character creation where you can pick your class and race
        this.assignedCharacter.setPlayerLocationReference(1);
        this.assignedCharacter.setPlayerSpawnPoint(1);
        try {
            boolean classSelection = true;
            boolean raceSelection = true;
            while (classSelection == true) {
                clientHandler.BW
                        .write("Creating a new account. What Class would you like your character to be?" + "\n");
                clientHandler.BW.newLine();
                clientHandler.BW.flush();
                String playerClassChoice = BR.readLine();
                if (playerClassChoice.toUpperCase().equals("MECHANIC")
                        || playerClassChoice.toUpperCase().equals("MERCENARY")
                        || playerClassChoice.toUpperCase().equals("PSION")
                        || playerClassChoice.toUpperCase().equals("SCOUT")) {
                    clientHandler.BW.write("Are you sure you want to pick the" + " " + playerClassChoice + " "
                            + "class?" + " " + "Y/N" + "\n");
                    clientHandler.BW.newLine();
                    clientHandler.BW.flush();
                    String confirmation = BR.readLine();
                    if (confirmation.equals(("Y")) || confirmation.equals("y")) {
                        classSelection = false;
                        switch (playerClassChoice.toUpperCase()) {
                            case "MECHANIC":
                                this.assignedCharacter.setPlayerClass(new Mechanic());
                                break;
                            case "MERCENARY":
                                this.assignedCharacter.setPlayerClass(new Mercenary());
                                break;
                            case "PSION":
                                this.assignedCharacter.setPlayerClass(new Psion());
                                break;
                            case "SCOUT":
                                this.assignedCharacter.setPlayerClass(new Scout());
                                break;
                        }
                    }
                } else {
                    clientHandler.BW.write("Sorry that's not a valid class, could you try again?");
                    clientHandler.BW.newLine();
                    clientHandler.BW.flush();
                }
            }

            while (raceSelection == true) {
                clientHandler.BW.write("Class selected. What race would you like?" + "\n");
                clientHandler.BW.newLine();
                clientHandler.BW.flush();
                String playerRaceChoice = BR.readLine();
                playerRaceChoice = playerRaceChoice.toUpperCase();
                if (playerRaceChoice.equals("CYBORG") || playerRaceChoice.equals("HUMAN")
                        || playerRaceChoice.equals("MUTANT") || playerRaceChoice.equals("STARBORN")) {
                    clientHandler.BW.write("Are you sure you want to pick the" + " " + playerRaceChoice + " " + "race?"
                            + " " + "Y/N" + "\n");
                    clientHandler.BW.newLine();
                    clientHandler.BW.flush();
                    String confirmation = BR.readLine();
                    if (confirmation.equals(("Y")) || confirmation.equals("y")) {
                        raceSelection = false;
                        switch (playerRaceChoice) {
                            case "CYBORG":
                                this.assignedCharacter.setPlayerRace(new Cyborg());
                                break;
                            case "HUMAN":
                                this.assignedCharacter.setPlayerRace(new Human());
                                break;
                            case "MUTANT":
                                this.assignedCharacter.setPlayerRace(new Mutant());
                                break;
                            case "STARBORN":
                                this.assignedCharacter.setPlayerRace(new Starborn());
                                break;
                        }
                    }
                } else {
                    clientHandler.BW.write("Sorry that's not a valid race, could you try again?");
                    clientHandler.BW.newLine();
                    clientHandler.BW.flush();
                }
            }

            clientHandler.BW.write(this.assignedCharacter.displayPlayerInformation());
            clientHandler.BW.newLine();
            clientHandler.BW.flush();
            savePlayerToFile(this.assignedCharacter);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public Player getPlayer(String playerName, String playerPassword) {
        // checks whether user details exist or not
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("players.dat"))) {
            while (true) {
                Player player = (Player) ois.readObject();
                if (player.getPlayerName().equals(playerName) && player.getPlayerPassword().equals(playerPassword)) {
                    return player;
                }
            }
        } catch (EOFException e) {
            return null;
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean checkAccountActivity(String username, String password, int ID) {
        // check whether user is already logged on
        boolean accountActive = false;
        for (ClientHandler clientHandler : mainServer.clientHandlers) {
            try {
                if (clientHandler.ID != ID) {
                    if ((clientHandler.assignedCharacter.getPlayerName().equals(username))
                            && (clientHandler.assignedCharacter.getPlayerPassword().equals(password))) {
                        accountActive = true;
                    }
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        return accountActive;
    }

    public void broadcastMessage(String messageToSend) {
        // send message to all other users other than itself
        for (ClientHandler clientHandler : mainServer.clientHandlers) {
            try {
                if (clientHandler.ID != this.ID) { // sends message to all other clients (not itself)

                    clientHandler.BW.write(messageToSend);
                    clientHandler.BW.newLine();
                    clientHandler.BW.flush();

                }

            } catch (IOException e) {
                closeEverything(socket, BR, BW);
            }
        }
    }

    public void removeClientHandler() throws IOException {
        // remove client handler on disconnect
        mainServer.clientHandlers.remove(this);
        broadcastMessage("User has disconnected");
    }

    public void closeEverything(Socket socket, BufferedReader BR, BufferedWriter BW) {

        // function to handle closing
        try {
            updatePlayer(assignedCharacter);
            removeClientHandler();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        try {
            if (BR != null) {
                BR.close();
            }
            if (BW != null) {
                BW.close();
            }
            if (socket != null) {
                socket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void updatePlayer(Player player) throws IOException {
        try {
            // Read all players from the players.dat file
            ArrayList<Player> players = (ArrayList<Player>) readPlayersFromFile();

            // Find the player to update and replace it with the new player data
            for (int i = 0; i < players.size(); i++) {
                if (players.get(i).getPlayerName().equals(player.getPlayerName())) {
                    players.set(i, player);
                    break;
                }
            }

            // Write all players back to the players.dat file
            writePlayersToFile(players);
        } catch (IOException e) {
            throw new IOException("Failed to update player data", e);
        }
    }

    private List<Player> readPlayersFromFile() throws IOException {
        List<Player> players = new ArrayList<>();
        try {

            Player player;
            FileInputStream fileIn = new FileInputStream("players.dat");
            ObjectInputStream objectIn = new ObjectInputStream(fileIn);

            while ((player = (Player) objectIn.readObject()) != null) {
                // Add the player to the list of players
                players.add(player);

            }
        } catch (EOFException e) {
            // Reached the end of the file
        } catch (IOException | ClassNotFoundException e) {
            throw new IOException("Failed to read player data", e);
        }
        return players;
    }

    private void writePlayersToFile(ArrayList<Player> players) throws IOException {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("players.dat"))) {
            for (Player player : players) {
                oos.writeObject(player);
            }
        }
    }

    // level up function
    public void levelUp(ClientHandler clientHandler) throws IOException {
        if (assignedCharacter.getExperience() >= assignedCharacter.getExperienceThreshold()) {
            assignedCharacter.playerClass.levelUp(assignedCharacter);
            // loop through each ability in the players class (classAbilities), check if the
            // minimum level is equal to the players current level, if it is, add it to the
            // players abilities
            for (ClassAbilities ability : assignedCharacter.playerClass.getClassAbilities()) {
                if (ability.getMinLevel() == assignedCharacter.getLevel()) {
                    assignedCharacter.classAbilities.add(ability);
                    clientHandler.BW.write("You have learned a new ability: " + ability.getName());
                    clientHandler.BW.newLine();
                    clientHandler.BW.flush();
                }
            }

            clientHandler.BW.write("You have leveled up!");
            clientHandler.BW.newLine();
            clientHandler.BW.flush();

            if (assignedCharacter.getQuest() == 5) {
                assignedCharacter.setWallet(assignedCharacter.getWallet() + 50);
                assignedCharacter.setExperience(assignedCharacter.getExperience() + 100);
                clientHandler.BW.write(
                        "You have completed the quest 'Level up'. You have been rewarded 50 credits and 100 experience");
                clientHandler.BW.newLine();
                clientHandler.BW.flush();
                assignedCharacter.clearQuest();
            }

            // unequip and re-equip all items to update stats
            if (assignedCharacter.equippedWeapon != null) {
                Weapon placeHolderWeapon = assignedCharacter.equippedWeapon;
                assignedCharacter.removeEquippedWeapon();
                assignedCharacter.setEquippedWeapon(placeHolderWeapon);
            }
            // do the same for chest, helmet and legs
            if (assignedCharacter.equippedChest != null) {
                Chest placeHolderChest = (Chest) assignedCharacter.equippedChest;
                assignedCharacter.removeEquippedChest();
                assignedCharacter.setEquippedChest((Chest) placeHolderChest);
            }
            if (assignedCharacter.equippedHelmet != null) {
                Helmet placeHolderHelmet = (Helmet) assignedCharacter.equippedHelmet;
                assignedCharacter.removeEquippedHelmet();
                assignedCharacter.setEquippedHelmet((Helmet) placeHolderHelmet);
            }
            if (assignedCharacter.equippedLegs != null) {
                Legs placeHolderLegs = (Legs) assignedCharacter.equippedLegs;
                assignedCharacter.removeEquippedLegs();
                assignedCharacter.setEquippedLegs((Legs) placeHolderLegs);
            }
        }
    }

    // give experience function for debugging
    public void giveExperience(int experience) {
        assignedCharacter.setExperience(assignedCharacter.getExperience() + experience);
    }
}
