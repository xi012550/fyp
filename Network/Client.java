package Network;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Client extends JFrame implements ActionListener {
    private static final long serialVersionUID = 1L;
    private JTextField inputField;
    private JButton sendButton;
    private JTextArea messageArea;
    private Socket socket;
    private BufferedReader reader;
    private BufferedWriter writer;

    public Client(Socket socket) {
        super("Wastland Survivor");
        this.socket = socket;
        try {
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        messageArea = new JTextArea();
        messageArea.setEditable(false);
        messageArea.setBackground(Color.BLACK);
        messageArea.setForeground(Color.WHITE);
        messageArea.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        add(new JScrollPane(messageArea), BorderLayout.CENTER);

        JPanel panel = new JPanel(new BorderLayout());

        inputField = new JTextField();
        inputField.setBackground(Color.BLACK);
        inputField.setForeground(Color.WHITE);
        inputField.addActionListener(this);
        panel.add(inputField, BorderLayout.CENTER);

        sendButton = new JButton("Send");
        sendButton.addActionListener(this);
        panel.add(sendButton, BorderLayout.EAST);

        panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        add(panel, BorderLayout.SOUTH);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(800, 600); // Set the size of the JFrame to 800x600 pixels.
        setVisible(true);

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        String message = reader.readLine();
                        if (message == null) {
                            close();
                            break;
                        }

                        if (message.startsWith("Server: ")) {
                            messageArea.append(message.substring(8) + "\n");
                            messageArea.setForeground(Color.WHITE);
                        } else {
                            messageArea.append(message + "\n");
                            messageArea.setForeground(Color.GREEN);
                        }

                        // Auto-scroll to the bottom of the text area
                        messageArea.setCaretPosition(messageArea.getDocument().getLength());

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == inputField || e.getSource() == sendButton) {
            try {
                String message = inputField.getText();
                writer.write(message);
                writer.newLine();
                writer.flush();
                messageArea.append("You: " + message + "\n");
                messageArea.setForeground(Color.GREEN);
                inputField.setText("");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    private void close() {
        try {
            if (reader != null) {
                reader.close();
            }
            if (writer != null) {
                writer.close();
            }
            if (socket != null) {
                socket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        try {
            Socket socket = new Socket("localhost", 1234);
            new Client(socket);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
