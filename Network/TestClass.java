package Network;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class TestClass extends JFrame implements ActionListener {
    private static final long serialVersionUID = 1L;
    private JTextField textField;
    private JButton button;
    private JLabel label;
    private JPanel panel;
    private JTextField outputField;

    public TestClass() {
        panel = new JPanel();
        panel.setLayout(new FlowLayout());
        panel.setBackground(Color.BLACK);
        add(panel);

        label = new JLabel("Enter text here: ");
        label.setForeground(Color.BLACK);
        panel.add(label);

        textField = new JTextField(10);
        textField.setForeground(Color.BLACK);
        panel.add(textField);

        button = new JButton("Click me");
        panel.add(button);
        button.addActionListener(this);

        outputField = new JTextField(10);
        outputField.setEditable(false);
        outputField.setBackground(Color.BLACK);
        outputField.setForeground(Color.WHITE);
        outputField.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        panel.add(outputField);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == button) {
            outputField.setText("hello there");
        }
    }

    public static void main(String[] args) {
        TestClass gui = new TestClass();
        gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        gui.setTitle("Test GUI");
        gui.setSize(300, 200);
        gui.setVisible(true);
    }
}