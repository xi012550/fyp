package Network;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

import World.Dungeon;
import World.Map;
import World.Room;

public class Server {

    private ServerSocket SC;

    // An array list to keep track of all the connected clients
    public static ArrayList<ClientHandler> clientHandlers = new ArrayList<>();

    // A map to store the game world
    public Map map = new Map();

    // A counter to keep track of the IDs assigned to clients
    private int id;

    // Constructor that takes a ServerSocket as an argument
    public Server(ServerSocket SC) {
        this.SC = SC;
    }

    public void startServer() {

        File playersFile = new File("players.dat");
        if (!playersFile.exists()) {
            try {
                playersFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // Create a temporary map object to initialize the game world
        Map tempMap = new Map();

        // check if world details file exists, if it does assign it to the map object
        // if it doesn't, create a new map object and initialize the game world
        if (new File("World Details.dat").exists()) {
            try (FileInputStream fis = new FileInputStream("World Details.dat");
                    ObjectInputStream ois = new ObjectInputStream(fis)) {
                tempMap = (Map) ois.readObject();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }

        } else {
            tempMap = new Map();
            tempMap.initialiseWorld();

            // Serialize the initialized game world to a file
            try (FileOutputStream fos = new FileOutputStream("World Details.dat");
                    ObjectOutputStream oos = new ObjectOutputStream(fos)) {
                oos.writeObject(tempMap);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        // Set the game world to the initialized world and reset the ID counter
        map = tempMap;
        id = 0;

        Thread gameLoopThread = new Thread(() -> {
            int loopCounter = 0;
            while (true) {
                // Increment the loop counter
                loopCounter++;

                // Perform an action every second
                try {
                    updateWorld(map);
                    for (ClientHandler CH : clientHandlers) {
                        CH.levelUp(CH);
                        // CH.updatePlayer(CH.assignedCharacter);
                    }
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                // Perform an action every 10 seconds
                if (loopCounter % 1 == 0) {
                    // for each dungeon in the map file, check if there is a player in the dungeon
                    // using the dungeon ID. if there is, refresh the entire dungeon
                    // for every dungeon that shares the same ID as the dungeon that the player is
                    // in, refresh the dungeon
                    // you can find the players current location using the player location reference
                    // alongside the room ID. the dungeon ID is specifically to categorise the
                    // entire dungeon
                    // the hasplayer function determines whether a player exists in a dungeon
                    for (Room room : map.worldList) {
                        if (room instanceof Dungeon) {
                            if (room.hasPlayer() != true) {
                                for (Room dungeon : map.worldList) {
                                    if (dungeon.getDungeonID() == room.getDungeonID()) {
                                        dungeon.refresh();
                                    }
                                }
                            }
                        }
                    }

                }

                // Sleep for a fixed duration between game loop iterations
                try {
                    Thread.sleep(1000); // 1 second
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        gameLoopThread.start();

        // Start listening for new connections and create a new client handler for each
        // one
        try {
            while (true) {
                Socket socket = SC.accept();
                System.out.println("A new client has connected!");
                id++;
                ClientHandler CH = new ClientHandler(socket, id, this);
                clientHandlers.add(CH);
                Thread thread = new Thread(CH);
                thread.start();
            }
        } catch (IOException e) {

        }

    }

    // Method to close the server socket
    public void closeServerSocket() {
        try {
            if (SC != null) {
                SC.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void updateWorld(Map world) throws IOException {
        try {
            // overwrite world details.dat file with new world details
            try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream("World Details.dat"))) {
                outputStream.writeObject(world);
            } catch (IOException e) {
                throw new IOException("Failed to save world data", e);
            }
        } catch (IOException e) {
            throw new IOException("Failed to update world data", e);
        }
    }

    public static void main(String[] args) throws IOException {
        // Create a new server socket and start the server
        ServerSocket SC = new ServerSocket(1234);
        Server server = new Server(SC);
        server.startServer();
    }
}
