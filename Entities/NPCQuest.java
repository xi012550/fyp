package Entities;

public class NPCQuest extends NPC implements java.io.Serializable {

    public String name;
    public String role;
    private String introduction;

    public NPCQuest(String name, String introduction) {
        super(name, "Quest", introduction);
    }

}
