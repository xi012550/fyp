package Entities;

import java.util.ArrayList;

import Objects.Items;

public class NPCMerchant extends NPC implements java.io.Serializable {

    // create an NPC with a name and a role

    public String name;
    public String role;
    private String introduction;
    // an arraylist of items that the merchant has for sale
    public ArrayList<Items> itemsForSale = new ArrayList<Items>();

    public NPCMerchant(String name, String introduction, ArrayList<Items> itemsForSale) {
        super(name, "Merchant", introduction);
        this.itemsForSale = itemsForSale;
    }

    // create get inventory method returning a string
    public String getInventory() {
        StringBuilder sb = new StringBuilder();
        for (Items item : itemsForSale) {
            sb.append(item.getName());
            sb.append("\n");
        }
        return sb.toString();
    }

    // return an arraylist of items that the merchant has for sale
    public ArrayList<Items> getInventoryArrayList() {
        return itemsForSale;
    }

}
