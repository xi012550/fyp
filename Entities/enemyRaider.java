package Entities;

import java.io.Serializable;

import Objects.LeatherChestpiece;
import Objects.LeatherHelmet;
import Objects.LeatherLegs;
import Objects.Machete;

public class enemyRaider extends Enemy implements Serializable {

    public enemyRaider(int level, int enemyID) {

        super(2, 10, 0.95, 0.95, 5, 20, level, enemyID);
        this.loot.add(new Machete(1));
        this.loot.add(new LeatherHelmet());
        this.loot.add(new LeatherChestpiece());
        this.loot.add(new LeatherLegs());

        if (level > 1) {
            for (int i = 1; i < level; i++) {
                this.damage = (int) Math.round(this.damage * 1.2);
                this.health = (int) Math.round(this.health * 1.2);
                this.speed = (int) Math.round(this.speed * 1.2);
                this.experienceGiven = (int) Math.round(this.experienceGiven * 1.5);
            }
        }
        this.name = "Raider";
    }
}
