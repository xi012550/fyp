package Entities;

import java.io.Serializable;
import java.util.ArrayList;

import Objects.Items;

public class Enemy implements Serializable {

    public int damage;
    public int health;
    public double defence;
    public double evasion;
    public int speed;
    public int level;
    public int enemyID;
    public int experienceGiven;
    public String name;
    public int turnOrder;
    public ArrayList<Items> loot = new ArrayList<Items>();

    public Enemy(int newDamage, int newHealth, double newDefence, double newEvasion, int newSpeed, int experienceGiven,
            int newLevel, int newEnemyID) {
        this.damage = newDamage;
        this.health = newHealth;
        this.defence = newDefence;
        this.evasion = newEvasion;
        this.speed = newSpeed;
        this.experienceGiven = experienceGiven;
        this.level = newLevel;
        this.enemyID = newEnemyID;

    }

    public int getDamage() {
        return damage;
    }

    public int getHealth() {
        return health;
    }

    public double getDefence() {
        return defence;
    }

    public double getEvasion() {
        return evasion;
    }

    public int getSpeed() {
        return speed;
    }

    public int getLevel() {
        return level;
    }

    public String getEnemyName() {
        return name;
    }

    public int getTurnOrder() {
        return turnOrder;
    }

    public int getEnemyID() {
        return enemyID;
    }

    public void setDamage(int newDamage) {
        this.damage = newDamage;
    }

    public void setHealth(int newHealth) {
        this.health = newHealth;
    }

    public void setDefence(double newDefence) {
        this.defence = newDefence;
    }

    public void setEvasion(int newEvasion) {
        this.evasion = newEvasion;
    }

    public void setSpeed(int newSpeed) {
        this.speed = newSpeed;
    }

    public void setLevel(int newLevel) {
        this.level = newLevel;
    }

    public int setTurnOrder(int newTurnOrder) {
        return this.turnOrder = newTurnOrder;
    }

    public int getExperience() {
        return this.experienceGiven;
    }

    public void addLoot(Items item) {
        loot.add(item);
    }

    public ArrayList<Items> getRandomLoot() {
        // returns a random item from the loot array
        ArrayList<Items> randomLoot = new ArrayList<Items>();
        int random = (int) (Math.random() * loot.size());
        randomLoot.add(loot.get(random));
        return randomLoot;
    }

}
