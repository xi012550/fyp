package Entities;

import java.util.ArrayList;

import Classes.BaseClass;
import Objects.Armour;
import Objects.Chest;
import Objects.Items;
import Objects.Weapon;
import Other.ClassAbilities;
import Quests.Quest;
import Races.Race;
import World.Room;

public class Player implements java.io.Serializable {

    // basic player information
    public String playerName;
    public String playerPassword;
    public BaseClass playerClass;
    public Race playerRace;
    public String playerCommand;
    public int playerLocationReference;
    public int playerSpawnPoint;
    public int turnOrder;
    public boolean inCombat = false;

    // player stats
    public int damage;
    public int maxHealth;
    public int health;
    public double defence;
    public double evasion;
    public int strength;
    public int dexterity;
    public int intelligence;
    public int speed;
    public int level;
    public int experience;
    public int experienceThreshold;

    // player inventory and equipped items
    public Weapon equippedWeapon;
    public Armour equippedHelmet;
    public Armour equippedChest;
    public Armour equippedLegs;
    public ArrayList<Items> inventory = new ArrayList<Items>();
    public int wallet;
    public ArrayList<ClassAbilities> classAbilities = new ArrayList<>();

    public Quest activeQuest;

    public String faction;

    // constructor
    public Player() {
        // initialize stats to 1
        this.damage = 1;
        this.maxHealth = 1;
        this.health = 1;
        this.defence = 1;
        this.evasion = 1;
        this.strength = 1;
        this.dexterity = 1;
        this.intelligence = 1;
        this.speed = 1;
        this.level = 1;
        this.experience = 0;
        this.experienceThreshold = 100;

        this.wallet = 10;

        this.activeQuest = new Quest(0, "No active quest");

        this.faction = "None";
    }

    // getters
    public String getPlayerName() {
        return playerName;
    }

    public String getPlayerPassword() {
        return playerPassword;
    }

    public String getPlayerClass() {
        return playerClass.getClassName();
    }

    public String getPlayerRace() {
        return playerRace.getRaceName();
    }

    public int getPlayerLocationReference() {
        return playerLocationReference;
    }

    public int getPlayerSpawnPoint() {
        return playerSpawnPoint;
    }

    public String getCommand() {
        return playerCommand;
    }

    public String getInventory() {
        String inventoryOutput = "In your inventory you have" + "\n";
        for (Items item : inventory) {
            inventoryOutput = inventoryOutput + (item.getName() + "\n");
        }
        return inventoryOutput;
    }

    public int getTurnOrder() {
        return turnOrder;
    }

    public Weapon getEquippedWeapon() {
        return equippedWeapon;
    }

    public Armour getEquippedHelmet() {
        return equippedHelmet;
    }

    public Armour getEquippedChest() {
        return equippedChest;
    }

    public Armour getEquippedLegs() {
        return equippedLegs;
    }

    public int getDamage() {
        return damage;
    }

    public int getMaxHealth() {
        return maxHealth;
    }

    public int getHealth() {
        return health;
    }

    public double getDefence() {
        return defence;
    }

    public double getEvasion() {
        return evasion;
    }

    public int getStrength() {
        return strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public int getSpeed() {
        return speed;
    }

    public int getLevel() {
        return level;
    }

    public int getExperience() {
        return experience;
    }

    public int getExperienceThreshold() {
        return experienceThreshold;
    }

    public int getWallet() {
        return wallet;
    }

    public int getRoom() {
        return playerLocationReference;
    }

    public String getFaction() {
        return faction;
    }

    // setters
    public void setPlayerName(String newName) {
        this.playerName = newName;
    }

    public void setInventory(ArrayList<Items> newInventory) {
        this.inventory = newInventory;
    }

    public void setPlayerPassword(String newPassword) {
        this.playerPassword = newPassword;
    }

    public void setPlayerClass(BaseClass newClass) {
        this.playerClass = newClass;
    }

    public void setPlayerRace(Race newRace) {
        this.playerRace = newRace;
    }

    public void setPlayerLocationReference(int Reference) {
        this.playerLocationReference = Reference;
    }

    public void setPlayerSpawnPoint(int spawnPoint) {
        this.playerSpawnPoint = spawnPoint;
    }

    public void setCommand(String command) {
        this.playerCommand = command;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public void setMaxHealth(int maxHealth) {
        this.maxHealth = maxHealth;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public void setDefence(double defence) {
        this.defence = defence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public void setEvasion(double evasion) {
        this.evasion = evasion;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public void setExperienceThreshold(int experienceThreshold) {
        this.experienceThreshold = experienceThreshold;
    }

    public void setWallet(int wallet) {
        this.wallet = wallet;
    }

    // methods
    public void setEquippedWeapon(Weapon weapon) {
        this.equippedWeapon = weapon;
        removeItem(weapon);
        this.damage = this.damage + weapon.getDamage();
        if (weapon.isRanged == true) {
            this.damage = this.damage + (int) Math.round(this.dexterity * 0.4);
        }
        if (weapon.isRanged == false) {
            this.damage = this.damage + (int) Math.round(this.strength * 0.4);
        }
        if (weapon.isRanged == null) {
        }
    }

    public void setEquippedHelmet(Armour helmet) {
        this.equippedHelmet = helmet;
        removeItem(helmet);
        this.defence = this.defence - helmet.getDefence();
    }

    public void setEquippedChest(Chest item) {
        this.equippedChest = item;
        removeItem(item);
        this.defence = this.defence - item.getDefence();
    }

    public void setEquippedLegs(Armour legs) {
        this.equippedLegs = legs;
        removeItem(legs);
        this.defence = this.defence - legs.getDefence();
    }

    public void setFaction(String faction) {
        this.faction = faction;
    }

    // public void addClassAbility(ClassAbilities ability) {
    // this.classAbilities.add(ability);
    // }

    public void removeEquippedWeapon() {
        addItem(this.equippedWeapon);
        this.damage = 1;
        this.equippedWeapon = null;
    }

    public void removeEquippedHelmet() {
        addItem(this.equippedHelmet);
        this.defence = this.defence + this.equippedHelmet.getDefence();
        this.equippedHelmet = null;
    }

    public void removeEquippedChest() {
        addItem(this.equippedChest);
        this.defence = this.defence + this.equippedChest.getDefence();
        this.equippedChest = null;
    }

    public void removeEquippedLegs() {
        addItem(this.equippedLegs);
        this.defence = this.defence + this.equippedLegs.getDefence();
        this.equippedLegs = null;
    }

    public String displayPlayerInformation() {
        String information = ("This character is named " + getPlayerName() + ". They are a " + getPlayerRace() + " "
                + getPlayerClass() + "." + "\n");

        information = information + ("Faction: " + getFaction() + "\n");
        if (equippedWeapon != null) {
            information = information + ("They are holding a " + this.equippedWeapon.getName() + "\n");
        }
        // repeat this for helmet, chest, legs
        if (equippedHelmet != null) {
            information = information
                    + ("They are wearing a " + this.equippedHelmet.getName() + " on their head." + "\n");
        }
        if (equippedChest != null) {
            information = information
                    + ("They are wearing a " + this.equippedChest.getName() + " on their chest." + "\n");
        }
        if (equippedLegs != null) {
            information = information
                    + ("They are wearing a " + this.equippedLegs.getName() + " on their legs." + "\n");
        }

        information = information + ("They have " + this.wallet + " chips in their wallet." + "\n");

        information = information + ("Their stats are as follows:" + "\n");
        information = information + ("Damage: " + this.damage + "\n");
        information = information + ("Health: " + this.health + "\n");
        information = information + ("Defence: " + this.defence + "\n");
        information = information + ("Strength: " + this.strength + "\n");
        information = information + ("Dexterity: " + this.dexterity + "\n");
        information = information + ("Intelligence: " + this.intelligence + "\n");
        information = information + ("Evasion: " + this.evasion + "\n");
        information = information + ("Speed: " + this.speed + "\n");
        information = information + ("Level: " + this.level + "\n");
        information = information + ("Experience: " + this.experience + "out of " + experienceThreshold + "\n");

        information = information + ("\n" + "They have the following abilities:" + "\n");
        for (ClassAbilities ability : classAbilities) {
            information = information + (ability.getName() + "\n");
        }

        return information;
    }

    public void removeItem(Items item) {
        inventory.remove(item);
    }

    public void addItem(Items item) {
        inventory.add(item);
    }

    public void setTurnOrder(int turnOrder) {
        this.turnOrder = turnOrder;
    }

    public void assignQuest(Quest quest) {
        this.activeQuest = quest;
    }

    public void clearQuest() {
        Quest quest = new Quest(0, "No active quest");
        this.activeQuest = quest;
    }

    public int getQuest() {
        return this.activeQuest.getID();
    }

    public String getQuestDescription() {
        return this.activeQuest.getDescription();
    }

}
