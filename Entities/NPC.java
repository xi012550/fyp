package Entities;

public class NPC implements java.io.Serializable {

    // create an NPC with a name and a role

    public String name;
    public String role;
    private String introduction;

    public NPC(String name, String role, String introduction) {
        this.name = name;
        this.role = role;
        this.introduction = introduction;
    }

    public String getName() {
        return name;
    }

    public String getRole() {
        return role;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getIntroduction() {
        return introduction;
    }

    public String getInventory() {
        return null;
    }

}
