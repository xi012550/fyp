package Entities;

public class NPCTrainer extends NPC implements java.io.Serializable {

    public String name;
    public String role;
    private String introduction;

    public NPCTrainer(String name, String introduction) {
        super(name, "Trainer", introduction);
    }

}
