package Classes;

import java.io.Serializable;
import Entities.Player;
import Other.ClassAbilities;

public class Mechanic extends BaseClass implements Serializable {

    public Mechanic() {
        className = "Mechanic";
        healthPerLevel = 10;
        speedPerLevel = 3;
        strengthPerLevel = 1;
        dexterityPerLevel = 3;
        intelligencePerLevel = 3;

        classAbilities.add(new ClassAbilities("First-Aid", "Heal yourself or an ally", 3));
        classAbilities.add(new ClassAbilities("Blast", "Throw an experimental explosive at an enemy", 7));
        classAbilities.add(new ClassAbilities("Experimental warfare",
                "Deal double your usual damage but take 50% of it back at yourself", 15));

    }

    public void firstAid(Player user, Player target) {
        user.setHealth(user.getHealth() + (user.getIntelligence() * 4));
    }

    public void blast(Player user, Player target) {
        target.setHealth(target.getHealth() - (user.getIntelligence() * 5));
    }

    public void experimentalWarfare(Player user, Player target) {
        target.setHealth(target.getHealth() - (user.getDamage() * 2));
        user.setHealth((int) Math.round(user.getHealth() - (user.getDamage() * 0.5)));
    }

}
