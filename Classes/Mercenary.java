package Classes;

import java.io.Serializable;

public class Mercenary extends BaseClass implements Serializable {

    public Mercenary() {
        className = "Mercenary";

        healthPerLevel = 20;
        speedPerLevel = 3;
        strengthPerLevel = 5;
        dexterityPerLevel = 3;
        intelligencePerLevel = 1;
    }

}
