package Classes;

import java.io.Serializable;
import java.util.ArrayList;

import Entities.Player;
import Other.ClassAbilities;

public class BaseClass implements Serializable {
    public String className;

    public int healthPerLevel;
    public int speedPerLevel;
    public int strengthPerLevel;
    public int dexterityPerLevel;
    public int intelligencePerLevel;
    public ArrayList<ClassAbilities> classAbilities = new ArrayList<>();

    public BaseClass() {

    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String classNameName) {
        this.className = className;
    }

    public void levelUp(Player player) {
        player.setMaxHealth(player.getMaxHealth() + healthPerLevel);
        player.setHealth(player.getMaxHealth());
        player.setSpeed(player.getSpeed() + speedPerLevel);
        player.setStrength(player.getStrength() + strengthPerLevel);
        player.setDexterity(player.getDexterity() + dexterityPerLevel);
        player.setIntelligence(player.getIntelligence() + intelligencePerLevel);
        player.setLevel(player.getLevel() + 1);
        player.setExperienceThreshold(player.getExperienceThreshold() + 50);
        player.setExperience(0);
    }

    // get and set methods
    public int getClassAbilitiesSize() {
        return classAbilities.size();
    }

    public String getClassAbilityName(int index) {
        return classAbilities.get(index).getName();
    }

    public String getClassAbilityDescription(int index) {
        return classAbilities.get(index).getDescription();
    }

    public int getClassAbilityLevel(int index) {
        return classAbilities.get(index).getMinLevel();
    }

    // return list of abilities
    public ArrayList<ClassAbilities> getClassAbilities() {
        return classAbilities;
    }

}
