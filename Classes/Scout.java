package Classes;

import java.io.Serializable;

public class Scout extends BaseClass implements Serializable {

    public Scout() {
        className = "Scout";

        healthPerLevel = 10;
        speedPerLevel = 5;
        strengthPerLevel = 3;
        dexterityPerLevel = 5;
        intelligencePerLevel = 1;
    }

}
