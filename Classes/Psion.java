package Classes;

import java.io.Serializable;

public class Psion extends BaseClass implements Serializable {

    public Psion() {
        className = "Psion";

        healthPerLevel = 15;
        speedPerLevel = 3;
        strengthPerLevel = 1;
        dexterityPerLevel = 3;
        intelligencePerLevel = 5;

    }

}
