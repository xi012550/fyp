package Other;

import java.util.ArrayList;

public class ClassAbilities implements java.io.Serializable {
    private String name;
    private String description;
    private int minLevel;
    protected ArrayList<ClassAbilities> abilityList = new ArrayList<>();

    public ClassAbilities(String name, String description, int minLevel) {
        this.name = name;
        this.description = description;
        this.minLevel = minLevel;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getMinLevel() {
        return minLevel;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setMinLevel(int minLevel) {
        this.minLevel = minLevel;
    }

}
