package Other;

//import the necessary packages
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Random;
import java.net.Socket;

import Entities.NPC;
import Entities.NPCMerchant;
import Entities.Player;
import World.Dungeon;
import World.Room;
import Network.ClientHandler;
import Network.Server;
import Objects.Items;
import Objects.Weapon;
import Quests.Quest;
import Objects.Armour;
import Objects.Chest;
import Objects.Helmet;
import Objects.Legs;

public class Commands {

    public static void talkAction(ClientHandler clientHandler, String npcToBeTalkedTo, Server mainServer,
            Player assignedCharacter) throws IOException {
        for (Room Room : mainServer.map.worldList) {
            if (Room.id == assignedCharacter.playerLocationReference) {
                for (NPC npc : Room.getNPCs()) {
                    if (npc.name.equalsIgnoreCase(npcToBeTalkedTo)) {
                        clientHandler.BW.write(npc.getIntroduction());
                        clientHandler.BW.newLine();
                        clientHandler.BW.flush();
                        // check the npc's role
                        if (npc.getRole().equals("Merchant")) {
                            boolean transactionProcess = true;
                            // print out the merchant's inventory
                            // check if itemtobebought is in the merchant's inventory using getitemarraylist
                            // if it is, check if the player has enough credits to buy it
                            // if they do, run the sell item method
                            // if they don't, tell them they don't have enough credits
                            while (transactionProcess == true) {
                                clientHandler.BW.write(npc.getInventory());
                                clientHandler.BW.newLine();
                                clientHandler.BW.flush();
                                String itemToBeBought = clientHandler.BR.readLine();
                                if (itemToBeBought.equalsIgnoreCase("Exit")) {
                                    transactionProcess = false;
                                    itemToBeBought = "";
                                    break;
                                }
                                for (Items itemforsale : ((NPCMerchant) npc).getInventoryArrayList()) {
                                    if (itemforsale.getName().equalsIgnoreCase(itemToBeBought)) {
                                        // confirm purchase
                                        clientHandler.BW
                                                .write("Are you sure you'd like to buy " + itemforsale.getName()
                                                        + " for " + itemforsale.getItemValue() + " credits? (Y/N)");
                                        clientHandler.BW.newLine();
                                        clientHandler.BW.flush();
                                        String purchaseConfirmation = clientHandler.BR.readLine();
                                        purchaseConfirmation = purchaseConfirmation.toUpperCase();
                                        if (purchaseConfirmation.equalsIgnoreCase("Y")
                                                && assignedCharacter.getWallet() >= itemforsale.getItemValue()) {
                                            assignedCharacter.setWallet(
                                                    assignedCharacter.getWallet() - itemforsale.getItemValue());
                                            assignedCharacter.inventory.add(itemforsale);
                                            clientHandler.BW
                                                    .write("You have bought " + itemforsale.getName() + " for "
                                                            + itemforsale.getItemValue() + " credits and now have "
                                                            + assignedCharacter.getWallet() + " credits left.");
                                            clientHandler.BW.newLine();
                                            clientHandler.BW.flush();
                                            itemToBeBought = "";
                                            break;
                                        }
                                        if (purchaseConfirmation.equalsIgnoreCase("Y")
                                                && assignedCharacter.getWallet() < itemforsale.getItemValue()) {
                                            clientHandler.BW.write("You don't have enough currency for this item");
                                            clientHandler.BW.newLine();
                                            clientHandler.BW.flush();
                                            itemToBeBought = "";
                                            break;
                                        } else {
                                            clientHandler.BW.write("Another time, maybe.");
                                            clientHandler.BW.newLine();
                                            clientHandler.BW.flush();
                                            itemToBeBought = "";
                                            break;
                                        }
                                    }
                                }
                            }

                        }

                        if (npc.getRole().equals("Trainer")) {
                            // the trainer will offer to give the player 100 experience for 100 credits
                            // check if the player has enough credits

                            clientHandler.BW
                                    .write("I can offer you some training for 100 credits. You interested? (Y/N)");
                            clientHandler.BW.newLine();
                            clientHandler.BW.flush();
                            String purchaseConfirmation = clientHandler.BR.readLine();
                            purchaseConfirmation = purchaseConfirmation.toUpperCase();
                            if (purchaseConfirmation.equalsIgnoreCase("Y")
                                    && assignedCharacter.getWallet() >= 100) {
                                assignedCharacter.setWallet(assignedCharacter.getWallet() - 100);
                                assignedCharacter.setExperience(assignedCharacter.getExperience() + 100);
                                clientHandler.BW.write("You have bought 100 experience for 100 credits and now have "
                                        + assignedCharacter.getWallet() + " credits left.");
                                clientHandler.BW.newLine();
                                clientHandler.BW.flush();
                                if (assignedCharacter.getQuest() == 4) {
                                    assignedCharacter.setWallet(assignedCharacter.getWallet() + 50);
                                    assignedCharacter.setExperience(assignedCharacter.getExperience() + 100);
                                    clientHandler.BW.write(
                                            "You have completed the quest 'Receive training'. You have been rewarded 50 credits and 100 experience");
                                    clientHandler.BW.newLine();
                                    clientHandler.BW.flush();
                                    assignedCharacter.clearQuest();
                                }
                            }
                            if (purchaseConfirmation.equalsIgnoreCase("Y")
                                    && assignedCharacter.getWallet() < 100) {
                                clientHandler.BW.write("You don't have enough credits for me to train you.");
                                clientHandler.BW.newLine();
                                clientHandler.BW.flush();
                            } else {
                                clientHandler.BW.write("Another time, maybe.");
                                clientHandler.BW.newLine();
                                clientHandler.BW.flush();
                            }
                        }

                        if (npc.getRole().equals("Quest")) {
                            clientHandler.BW.write(
                                    "Do you want an errand to carry out? I'll make sure you get rewarded for it (Y/N)");
                            clientHandler.BW.newLine();
                            clientHandler.BW.flush();
                            String questConfirmation = clientHandler.BR.readLine();
                            questConfirmation = questConfirmation.toUpperCase();
                            if (questConfirmation.equalsIgnoreCase("Y") && (assignedCharacter.getQuest() == 0)) {
                                Quest Quest1 = new Quest(1, "Survive a combat encounter.");
                                Quest Quest2 = new Quest(2, "Find a new weapon. (can't be bought)");
                                Quest Quest3 = new Quest(3, "Find a new piece of armour. (can't be bought))");
                                Quest Quest4 = new Quest(4, "Receive Training.");
                                Quest Quest5 = new Quest(5, "Level up.");

                                // add all these quests to a list and then randomly select one to give to the
                                // player
                                ArrayList<Quest> QuestList = new ArrayList<Quest>();
                                QuestList.add(Quest1);
                                QuestList.add(Quest2);
                                QuestList.add(Quest3);
                                QuestList.add(Quest4);
                                QuestList.add(Quest5);

                                Random rand = new Random();
                                int randomQuest = rand.nextInt(QuestList.size());
                                Quest questToBeAssigned = QuestList.get(randomQuest);
                                assignedCharacter.assignQuest(questToBeAssigned);
                            }
                            // if quest is 0, tell the player they already have a quest
                            if (questConfirmation.equalsIgnoreCase("Y") && (assignedCharacter.getQuest() != 0)) {
                                clientHandler.BW
                                        .write("You already have a task. Do you really wanna abandon it? (Y/N)");
                                clientHandler.BW.newLine();
                                clientHandler.BW.flush();
                                String questAbandonConfirmation = clientHandler.BR.readLine();
                                questAbandonConfirmation = questAbandonConfirmation.toUpperCase();
                                if (questAbandonConfirmation.equalsIgnoreCase("Y")) {
                                    assignedCharacter.clearQuest();
                                    Quest Quest1 = new Quest(1, "Survive a combat encounter.");
                                    Quest Quest2 = new Quest(2, "Find a new weapon.");
                                    Quest Quest3 = new Quest(3, "Find a new piece of armour.");
                                    Quest Quest4 = new Quest(4, "Sell an item.");
                                    Quest Quest5 = new Quest(5, "Level up.");

                                    // add all these quests to a list and then randomly select one to give to the
                                    // player
                                    ArrayList<Quest> QuestList = new ArrayList<Quest>();
                                    QuestList.add(Quest1);
                                    QuestList.add(Quest2);
                                    QuestList.add(Quest3);
                                    QuestList.add(Quest4);
                                    QuestList.add(Quest5);

                                    Random rand = new Random();
                                    int randomQuest = rand.nextInt(QuestList.size());
                                    Quest questToBeAssigned = QuestList.get(randomQuest);
                                    assignedCharacter.assignQuest(questToBeAssigned);
                                } else {
                                    clientHandler.BW.write("You better get to it then.");
                                    clientHandler.BW.newLine();
                                    clientHandler.BW.flush();
                                }
                            } else {
                                clientHandler.BW.write("Another time, maybe.");
                                clientHandler.BW.newLine();
                                clientHandler.BW.flush();
                            }

                        }
                    }
                }
            }
        }
    }

    public static void helpAction(ClientHandler clientHandler) {
        try {
            clientHandler.BW.write(
                    "Help: Ask for a list and explanation of commands" + "\n" +
                            "Move: Move your player from one room to another using North, East, South, West, Up or Down"
                            + "\n" +
                            "Talk: Type NPC to talk to an NPC in the room you're in, FACTION to talk to your entire faction, and ROOM to speak to players in the same room as you"
                            + "\n" +
                            "Inventory: Show the items stored in your inventory" + "\n" +
                            "Look: See the contents of the room you're currently located in" + "\n" +
                            "Take: Take an item from the room and put it into your inventory" + "\n" +
                            "Equip: Attach an equippable item to your player character from your inventory such as a weapon or armour"
                            + "\n" +
                            "Unequip: Unequip an equipped item from your player character and put it back into your inventory"
                            + "\n" +
                            "Drop: Drop an item from your inventory into your current room" + "\n" +
                            "Self: Get information about your current state" + "\n" +
                            "Quest: Get information about your current quest" + "\n" +
                            "Set Spawn: Set your spawn point to the room you're currently in" + "\n" +
                            "Create Faction: Create a faction" + "\n"

            );
            clientHandler.BW.newLine();
            clientHandler.BW.flush();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void dropAction(ClientHandler clientHandler, String itemToBeDropped, Server mainServer,
            Player assignedCharacter) {
        // check which room the player is in and then drop the chosen item into it
        for (Room Room : mainServer.map.worldList) {
            if (Room.id == assignedCharacter.playerLocationReference) {
                for (Items item : assignedCharacter.inventory) {
                    if (item.getName().equalsIgnoreCase(itemToBeDropped)) {
                        Room.addObject(item);
                        assignedCharacter.removeItem(item);
                        break;
                    }
                }
            }
        }
    }

    public static void equipAction(ClientHandler clientHandler, String itemToBeEquipped, Player assignedCharacter) {
        // go through player inventory then equip object if its able to be equipped
        for (Items item : assignedCharacter.inventory) {
            if (item.getName().equalsIgnoreCase(itemToBeEquipped)) {
                if (item instanceof Weapon) {
                    // check if the player has a weapon equipped already
                    if (assignedCharacter.equippedWeapon != null) {
                        // if they do, add the equipped weapon to their inventory
                        assignedCharacter.inventory.add(assignedCharacter.equippedWeapon);
                        // equip the new weapon
                        assignedCharacter.setEquippedWeapon((Weapon) item);
                        // remove the new weapon from their inventory
                        assignedCharacter.removeItem(item);
                        break;
                    } else {
                        assignedCharacter.setEquippedWeapon((Weapon) item);
                        break;
                    }
                }
                // repeat this for the other types of equippable items
                if (item instanceof Helmet) {
                    if (assignedCharacter.equippedHelmet != null) {
                        assignedCharacter.inventory.add(assignedCharacter.equippedHelmet);
                        assignedCharacter.setEquippedHelmet((Helmet) item);
                        assignedCharacter.removeItem(item);
                        break;
                    } else {
                        assignedCharacter.setEquippedHelmet((Helmet) item);
                        break;
                    }
                }
                if (item instanceof Chest) {
                    if (assignedCharacter.equippedChest != null) {
                        assignedCharacter.inventory.add(assignedCharacter.equippedChest);
                        assignedCharacter.setEquippedChest((Chest) item);
                        assignedCharacter.removeItem(item);
                        break;
                    } else {
                        assignedCharacter.setEquippedChest((Chest) item);
                        break;
                    }
                }
                if (item instanceof Legs) {
                    if (assignedCharacter.equippedLegs != null) {
                        assignedCharacter.inventory.add(assignedCharacter.equippedLegs);
                        assignedCharacter.setEquippedLegs((Legs) item);
                        assignedCharacter.removeItem(item);
                        break;
                    } else {
                        assignedCharacter.setEquippedLegs((Legs) item);
                        break;
                    }
                }
            }
        }
    }

    public static void unequipAction(ClientHandler clientHandler, String itemToBeEquipped, Player assignedCharacter) {
        // unequip item if the player has one equipped
        if (assignedCharacter.equippedWeapon != null) {
            if (assignedCharacter.equippedWeapon.getName().equalsIgnoreCase(itemToBeEquipped)) {
                assignedCharacter.removeEquippedWeapon();
            }
        }

        if (assignedCharacter.equippedHelmet != null) {
            if (assignedCharacter.equippedHelmet.getName().equalsIgnoreCase(itemToBeEquipped)) {
                assignedCharacter.removeEquippedHelmet();
            }
        }

        if (assignedCharacter.equippedChest != null) {
            if (assignedCharacter.equippedChest.getName().equalsIgnoreCase(itemToBeEquipped)) {
                assignedCharacter.removeEquippedChest();
            }
        }

        if (assignedCharacter.equippedLegs != null) {
            if (assignedCharacter.equippedLegs.getName().equalsIgnoreCase(itemToBeEquipped)) {
                assignedCharacter.removeEquippedLegs();
            }
        }

    }

    public static void moveAction(ClientHandler clientHandler, String direction, Server mainServer,
            Player assignedCharacter) throws IOException {
        // check the rooms surrounding the one the player is currently in, and then move
        // accordingly
        for (Room Room : mainServer.map.worldList) {
            if (Room.id == assignedCharacter.playerLocationReference) {
                switch (direction) {
                    case "NORTH":
                        if (Room.getNorth() != null) {
                            Room newroom = Room.getNorth();
                            Room.removePlayer(assignedCharacter);
                            Room.getNorth().addPlayer(assignedCharacter);
                            assignedCharacter.playerLocationReference = newroom.id;
                            clientHandler.BW.write(newroom.getDescription() + "\n" + newroom.getExits() + "\n");
                            clientHandler.BW.newLine();
                            clientHandler.BW.flush();

                        }
                        break;
                    case "EAST":
                        if (Room.getEast() != null) {
                            Room newroom = Room.getEast();
                            Room.removePlayer(assignedCharacter);
                            Room.getEast().addPlayer(assignedCharacter);
                            assignedCharacter.playerLocationReference = newroom.id;
                            clientHandler.BW.write(newroom.getDescription() + "\n" + newroom.getExits() + "\n");
                            clientHandler.BW.newLine();
                            clientHandler.BW.flush();
                        }
                        break;
                    case "SOUTH":
                        if (Room.getSouth() != null) {
                            Room newroom = Room.getSouth();
                            Room.removePlayer(assignedCharacter);
                            Room.getSouth().addPlayer(assignedCharacter);
                            assignedCharacter.playerLocationReference = newroom.id;
                            clientHandler.BW.write(newroom.getDescription() + "\n" + newroom.getExits() + "\n");
                            clientHandler.BW.newLine();
                            clientHandler.BW.flush();
                        }
                        break;
                    case "WEST":
                        if (Room.getWest() != null) {
                            Room newroom = Room.getWest();
                            Room.removePlayer(assignedCharacter);
                            Room.getWest().addPlayer(assignedCharacter);
                            assignedCharacter.playerLocationReference = newroom.id;
                            clientHandler.BW.write(newroom.getDescription() + "\n" + newroom.getExits() + "\n");
                            clientHandler.BW.newLine();
                            clientHandler.BW.flush();
                        }
                        break;
                    case "UP":
                        if (Room.getUp() != null) {
                            Room newroom = Room.getUp();
                            Room.removePlayer(assignedCharacter);
                            Room.getUp().addPlayer(assignedCharacter);
                            assignedCharacter.playerLocationReference = newroom.id;
                            clientHandler.BW.write(newroom.getDescription() + "\n" + newroom.getExits() + "\n");
                            clientHandler.BW.newLine();
                            clientHandler.BW.flush();
                        }
                        break;
                    case "DOWN":
                        if (Room.getDown() != null) {
                            Room newroom = Room.getDown();
                            Room.removePlayer(assignedCharacter);
                            Room.getDown().addPlayer(assignedCharacter);
                            assignedCharacter.playerLocationReference = newroom.id;
                            clientHandler.BW.write(newroom.getDescription() + "\n" + newroom.getExits() + "\n");
                            clientHandler.BW.newLine();
                            clientHandler.BW.flush();
                        }
                        break;

                }
                break;
            }
        }
    }

    // make a questAction method that outputs the players assigned quest description
    // (specifically from the players assignedQuest attribute)

    public static void questAction(ClientHandler clientHandler, Player assignedCharacter) {
        try {
            clientHandler.BW.write(assignedCharacter.getQuestDescription() + "\n");
            clientHandler.BW.newLine();
            clientHandler.BW.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void lookAction(ClientHandler clientHandler, Server mainServer, Player assignedCharacter) {
        // Get details and description of room player is currently in
        for (Room Room : mainServer.map.worldList) {
            if (Room.id == assignedCharacter.playerLocationReference) {
                try {
                    String roomDescription = Room.getDescription();
                    clientHandler.BW.write(roomDescription);
                    clientHandler.BW.newLine();
                    clientHandler.BW.flush();
                    break;
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    // make a method called "setSpawnPoint". this method will modify the players
    // spawn point to the room they are currently in, as long as its not a dungeon
    public static void setSpawnPoint(ClientHandler clientHandler, Server mainServer, Player assignedCharacter) {
        for (Room Room : mainServer.map.worldList) {
            if (Room.id == assignedCharacter.playerLocationReference) {
                if (!(Room instanceof Dungeon)) {
                    assignedCharacter.setPlayerSpawnPoint(Room.id);
                    break;
                }
            }
        }
    }

    public static void takeAction(ClientHandler clientHandler, String inputItem, Server mainServer,
            Player assignedCharacter) throws IOException {
        // take object from inside of the players room and add it to their inventory
        for (Room Room : mainServer.map.worldList) {
            if (Room.id == assignedCharacter.playerLocationReference) {
                for (Items item : Room.getItems()) {
                    if (item.getName().equalsIgnoreCase(inputItem)) {
                        Room.removeObject(item);
                        assignedCharacter.inventory.add(item);
                        // check if item is a weapon, if so, equip it
                        if (item instanceof Weapon) {
                            if (assignedCharacter.getQuest() == 2) {
                                assignedCharacter.setWallet(assignedCharacter.getWallet() + 50);
                                assignedCharacter.setExperience(assignedCharacter.getExperience() + 100);
                                clientHandler.BW.write(
                                        "You have completed the quest 'Find a new weapon'. You have been rewarded 50 credits and 100 experience");
                                clientHandler.BW.newLine();
                                clientHandler.BW.flush();
                                assignedCharacter.clearQuest();
                            }
                        }
                        if (item instanceof Armour) {
                            if (assignedCharacter.getQuest() == 3) {
                                assignedCharacter.setWallet(assignedCharacter.getWallet() + 50);
                                assignedCharacter.setExperience(assignedCharacter.getExperience() + 100);
                                clientHandler.BW.write(
                                        "You have completed the quest 'Find a new piece of armour'. You have been rewarded 50 credits and 100 experience");
                                clientHandler.BW.newLine();
                                clientHandler.BW.flush();
                                assignedCharacter.clearQuest();
                            }
                        }
                        break;
                    }
                }
                break;
            }
        }
    }

    public static void createFaction(ClientHandler clientHandler) throws IOException {
        // ask the player if they'd like to create a faction
        try {
            clientHandler.BW.write("Would you like to create a faction? (Y/N)");
            clientHandler.BW.newLine();
            clientHandler.BW.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        String response = clientHandler.BR.readLine();

        // if the player says yes, create a faction by changing the name of their
        // faction attribute
        if (response.equalsIgnoreCase("Y")) {
            try {
                clientHandler.BW.write("What would you like to name your faction?");
                clientHandler.BW.newLine();
                clientHandler.BW.flush();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            try {
                String factionName = clientHandler.BR.readLine();
                clientHandler.assignedCharacter.setFaction(factionName);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public static void inviteFaction(ClientHandler clientHandler, Server server) throws IOException {
        // asks the user to input the name of the player they'd like to invite to their
        // faction
        clientHandler.BW.write("Who would you like to invite to your faction?");
        clientHandler.BW.newLine();
        clientHandler.BW.flush();
        String invitedPlayer = clientHandler.BR.readLine();
        // checks to see if the player they'd like to invite is online
        for (ClientHandler client : server.clientHandlers) {
            if (client.assignedCharacter.getPlayerName().equalsIgnoreCase(invitedPlayer)
                    && client.assignedCharacter.inCombat == false) {
                // if the player is online, ask them if they'd like to join the faction
                client.BW.write("You have been invited to join " + clientHandler.assignedCharacter.getFaction()
                        + ". Would you like to join? (Y/N)");
                client.BW.newLine();
                client.BW.flush();
                String response = client.BR.readLine();
                // if the player says yes, add them to the faction
                if (response.equalsIgnoreCase("Y")) {
                    client.assignedCharacter.setFaction(clientHandler.assignedCharacter.getFaction());
                    client.BW.write("You have joined " + clientHandler.assignedCharacter.getFaction() + "!");
                    client.BW.newLine();
                    client.BW.flush();
                }
            }
        }

    }
}
