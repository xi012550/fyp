package Other;

import Entities.Player;

public class MechanicAbilities extends ClassAbilities {

    public MechanicAbilities(String name, String description, int minLevel) {
        super(name, description, minLevel);
        abilityList.add(new ClassAbilities("First-Aid", "Heal yourself or an ally", 3));
        abilityList.add(new ClassAbilities("Blast", "Throw an experimental explosive at an enemy", 7));
        abilityList.add(new ClassAbilities("Experimental warfare",
                "Deal double your usual damage but take 50% of it back at yourself", 15));
    }

    public void firstAid(Player user, Player target) {
        user.setHealth(user.getHealth() + (user.getIntelligence() * 4));
    }

    public void blast(Player user, Player target) {
        target.setHealth(target.getHealth() - (user.getIntelligence() * 5));
    }

    public void experimentalWarfare(Player user, Player target) {
        target.setHealth(target.getHealth() - (user.getDamage() * 2));
        user.setHealth((int) Math.round(user.getHealth() - (user.getDamage() * 0.5)));
    }

}
