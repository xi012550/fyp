package Races;

import java.io.Serializable;

public class Race implements Serializable {
    public String raceName;

    // Default constructor for the Race class.
    public Race(){
    }

    // Getter method for the race name field.
    public String getRaceName() {
        return raceName;
    }

    // Setter method for the race name field.
    public void setRaceName(String raceName) {
        this.raceName = raceName;
    }
}