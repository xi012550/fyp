package Combat;

import java.io.IOException;
import java.util.ArrayList;

import Entities.Enemy;
import Entities.Player;
import Network.ClientHandler;
import Objects.Items;
import Other.ClassAbilities;

public class CombatLoop {

    public static boolean start(Player player, ArrayList<Enemy> enemies, ClientHandler clientHandler)
            throws IOException {

        // create a for loop that'll iterate through the enemies in the room

        boolean playerWin = true;

        clientHandler.BW.write("you are in combat with " + "\n");
        for (Enemy enemy : enemies) {
            clientHandler.BW.write(enemy.getLevel() + " " + enemy.getEnemyName() + " " + enemy.getEnemyID());
            clientHandler.BW.newLine();
            clientHandler.BW.flush();
        }

        int enemySize = enemies.size();
        int experienceGiven = 0;
        ArrayList<Items> tempLoot = new ArrayList<Items>();

        // set the player turn order and each enemy turn order to 0

        player.setTurnOrder(0);
        ;
        for (int i = 0; i < enemySize; i++) {
            enemies.get(i).setTurnOrder(0);
        }

        // compare each of the enemies speeds to eachother and assign them turn order

        for (int i = 0; i < enemySize; i++) {
            for (int j = 0; j < enemySize; j++) {
                if (enemies.get(i).getSpeed() > enemies.get(j).getSpeed()) {
                    enemies.get(j).setTurnOrder(enemies.get(j).getTurnOrder() + 1);
                }
            }
        }

        // compare the players speed to each of the enemies and assign them turn order
        // and change the enemies turn order accordingly.

        for (int i = 0; i < enemySize; i++) {
            if (player.speed > enemies.get(i).getSpeed()) {
                enemies.get(i).setTurnOrder(enemies.get(i).getTurnOrder() + 1);
            }
            if (player.speed < enemies.get(i).getSpeed()) {
                player.setTurnOrder(player.getTurnOrder() + 1);
            }
        }

        // depending on the player and enemy speed, output the order to the user using
        // clienthandler.bw.write

        int combatSize = enemySize + 1;

        // post the combat order to the user keep in mind that the combat size is larger
        // than the enemy size so it might cause an error

        clientHandler.BW.write("The combat order is: " + "\n");
        clientHandler.BW.newLine();
        clientHandler.BW.flush();

        for (int i = 0; i < combatSize; i++) {
            for (int j = 0; j < enemySize; j++) {
                if (enemies.get(j).getTurnOrder() == i) {
                    clientHandler.BW.write(enemies.get(j).getEnemyName());
                    clientHandler.BW.newLine();
                    clientHandler.BW.flush();
                }
            }
            if (player.getTurnOrder() == i) {
                clientHandler.BW.write(player.getPlayerName());
                clientHandler.BW.newLine();
                clientHandler.BW.flush();
            }
        }

        boolean combatActive = true;
        String combatCommand;

        while (combatActive) {

            for (int i = 0; i < combatSize; i++) {
                for (int j = 0; j < enemySize; j++) {
                    if (enemies.get(j).getTurnOrder() == i) {
                        // have the enemy attack the player using the basic enemy attack method found in
                        // combat.java
                        Combat.basicEnemyAttack(enemies.get(j), player);
                        // output the enemy attack to the user and the damage dealt
                        clientHandler.BW.write(enemies.get(j).getEnemyName() + " attacked you for "
                                + enemies.get(j).getDamage() + " damage" + "\n");
                        clientHandler.BW.newLine();
                        clientHandler.BW.flush();
                        // tell the player their current health
                        clientHandler.BW.write("Your current health is " + player.getHealth() + "\n");
                    }
                }

                if (player.getTurnOrder() == i && player.getHealth() > 0) {
                    Boolean playerTurn = true;
                    while (playerTurn) {
                        clientHandler.BW.write("Take your action");
                        clientHandler.BW.newLine();
                        clientHandler.BW.flush();
                        combatCommand = clientHandler.BR.readLine();
                        if (combatCommand.equalsIgnoreCase("attack")) {
                            clientHandler.BW.write("Which enemy are you attacking? (enter enemy ID)");
                            clientHandler.BW.newLine();
                            clientHandler.BW.flush();
                            for (Enemy enemy : enemies) {
                                clientHandler.BW.write("Level " +
                                        enemy.getLevel() + " " + enemy.getEnemyName() + " ID: " + enemy.getEnemyID());
                                clientHandler.BW.newLine();
                                clientHandler.BW.flush();
                            }

                            int Target = 0;
                            try {
                                String input = clientHandler.BR.readLine();
                                Target = Integer.parseInt(input);
                            } catch (NumberFormatException e) {
                                clientHandler.BW.write("Invalid input. Please enter an enemy ID");
                                clientHandler.BW.newLine();
                                clientHandler.BW.flush();
                                break;
                            } catch (IOException e) {
                            }
                            for (Enemy enemy : enemies) {
                                if (enemy.getEnemyID() == Target) {
                                    Combat.basicPlayerAttack(player, enemy);
                                    clientHandler.BW.write(enemy.getEnemyName() + " " + enemy.getEnemyID()
                                            + " now has " + enemy.getHealth() + " health");
                                    clientHandler.BW.newLine();
                                    clientHandler.BW.flush();
                                    if (enemy.getHealth() <= 0) {
                                        clientHandler.BW.write(enemy.getEnemyName() + " " + enemy.getEnemyID()
                                                + " has been defeated");
                                        clientHandler.BW.newLine();
                                        clientHandler.BW.flush();
                                        experienceGiven = experienceGiven + enemy.getExperience();
                                        tempLoot = enemy.getRandomLoot();
                                        enemies.remove(enemy);
                                        enemySize = enemySize - 1;
                                        combatSize = combatSize - 1;
                                    }
                                    playerTurn = false;
                                    break;
                                }
                            }
                        }

                        if (combatCommand.equalsIgnoreCase("abilities")) {
                            // output "which ability would you like to use?"" then show the abilities the
                            // player has
                            clientHandler.BW.write("Which ability would you like to use?");
                            clientHandler.BW.newLine();
                            clientHandler.BW.flush();
                            for (ClassAbilities ability : player.classAbilities) {
                                clientHandler.BW.write(ability.getName());
                                clientHandler.BW.newLine();
                                clientHandler.BW.flush();
                            }
                            // check what abilities the player has and then use the ability
                            String abilityCommand = clientHandler.BR.readLine();
                            for (ClassAbilities ability : player.classAbilities) {

                                if (abilityCommand.equalsIgnoreCase(ability.getName())) {
                                    if (abilityCommand.equalsIgnoreCase("first-aid")) {
                                        player.setHealth(player.getHealth() + (player.getIntelligence() * 4));
                                        if (player.getHealth() > player.getMaxHealth()) {
                                            player.setHealth(player.getMaxHealth());
                                        }
                                        clientHandler.BW.write("You have healed yourself for "
                                                + (player.getIntelligence() * 4) + " health and now have "
                                                + player.getHealth() + " health" + "\n");
                                        clientHandler.BW.newLine();
                                        clientHandler.BW.flush();
                                        playerTurn = false;
                                    }
                                    if (abilityCommand.equalsIgnoreCase("blast")) {
                                        clientHandler.BW.write("Which enemy are you attacking? (enter enemy ID)");
                                        clientHandler.BW.newLine();
                                        clientHandler.BW.flush();
                                        for (Enemy enemy : enemies) {
                                            clientHandler.BW.write("Level " + enemy.getLevel() + " "
                                                    + enemy.getEnemyName() + " ID: " + enemy.getEnemyID());
                                            clientHandler.BW.newLine();
                                            clientHandler.BW.flush();
                                        }

                                        int Target = 0;
                                        try {
                                            String input = clientHandler.BR.readLine();
                                            Target = Integer.parseInt(input);
                                        } catch (NumberFormatException e) {
                                            clientHandler.BW.write("Invalid input. Please enter an enemy ID");
                                            clientHandler.BW.newLine();
                                            clientHandler.BW.flush();
                                            break;
                                        } catch (IOException e) {
                                        }
                                        for (Enemy enemy : enemies) {
                                            if (enemy.getEnemyID() == Target) {
                                                enemy.setHealth(enemy.getHealth()
                                                        - (clientHandler.assignedCharacter.getIntelligence() * 2));
                                                clientHandler.BW.write(enemy.getEnemyName() + " " + enemy.getEnemyID()
                                                        + " now has " + enemy.getHealth() + " health");
                                                clientHandler.BW.newLine();
                                                clientHandler.BW.flush();
                                                if (enemy.getHealth() <= 0) {
                                                    clientHandler.BW
                                                            .write(enemy.getEnemyName() + " " + enemy.getEnemyID()
                                                                    + " has been defeated");
                                                    clientHandler.BW.newLine();
                                                    clientHandler.BW.flush();
                                                    experienceGiven = experienceGiven + enemy.getExperience();
                                                    tempLoot = enemy.getRandomLoot();
                                                    enemies.remove(enemy);
                                                    enemySize = enemySize - 1;
                                                    combatSize = combatSize - 1;
                                                }
                                                playerTurn = false;
                                                break;
                                            }
                                        }
                                    }

                                    if (abilityCommand.equalsIgnoreCase("experimental warfare")) {
                                        clientHandler.BW.write("Which enemy are you attacking? (enter enemy ID)");
                                        clientHandler.BW.newLine();
                                        clientHandler.BW.flush();
                                        for (Enemy enemy : enemies) {
                                            clientHandler.BW.write("Level " + enemy.getLevel() + " "
                                                    + enemy.getEnemyName() + " ID: " + enemy.getEnemyID());
                                            clientHandler.BW.newLine();
                                            clientHandler.BW.flush();
                                        }

                                        int Target = 0;
                                        try {
                                            String input = clientHandler.BR.readLine();
                                            Target = Integer.parseInt(input);
                                        } catch (NumberFormatException e) {
                                            clientHandler.BW.write("Invalid input. Please enter an enemy ID");
                                            clientHandler.BW.newLine();
                                            clientHandler.BW.flush();
                                            break;
                                        } catch (IOException e) {
                                        }
                                        for (Enemy enemy : enemies) {
                                            if (enemy.getEnemyID() == Target) {
                                                enemy.setHealth(enemy.getHealth()
                                                        - (clientHandler.assignedCharacter.getDamage() * 3));
                                                player.setHealth(player.getHealth() - (player.getDamage() / 2));
                                                clientHandler.BW.write(enemy.getEnemyName() + " " + enemy.getEnemyID()
                                                        + " now has " + enemy.getHealth() + " health");
                                                clientHandler.BW.newLine();
                                                clientHandler.BW.flush();
                                                if (enemy.getHealth() <= 0) {
                                                    clientHandler.BW
                                                            .write(enemy.getEnemyName() + " " + enemy.getEnemyID()
                                                                    + " has been defeated");
                                                    clientHandler.BW.newLine();
                                                    clientHandler.BW.flush();
                                                    experienceGiven = experienceGiven + enemy.getExperience();
                                                    tempLoot = enemy.getRandomLoot();
                                                    enemies.remove(enemy);
                                                    enemySize = enemySize - 1;
                                                    combatSize = combatSize - 1;
                                                }
                                                playerTurn = false;
                                                break;
                                            }
                                        }
                                    }

                                }
                                // if the ability doesn't exist, tell the user to enter a valid ability
                                else {
                                    clientHandler.BW.write("Invalid input. Please enter a valid ability");
                                    clientHandler.BW.newLine();
                                    clientHandler.BW.flush();
                                }
                            }

                        }
                    }
                }
            }
            // if all enemies are dead, end combat
            if (enemySize == 0) {
                combatActive = false;
                playerWin = true;
                player.setExperience(player.getExperience() + experienceGiven);
                // output how much experience the player has gained
                clientHandler.BW.write("You have gained " + experienceGiven + " experience");
                clientHandler.BW.newLine();
                clientHandler.BW.flush();
                // for each item in temploot, give the player the item
                for (Items item : tempLoot) {
                    player.inventory.add(item);
                    clientHandler.BW.write("You have gained " + item.getName());
                    clientHandler.BW.newLine();
                    clientHandler.BW.flush();
                }

                // check if the player has quest of ID 1, if they do complete the quest and give
                // them 100 chips and 50 experience
                if (player.getQuest() == 1) {
                    player.setWallet(player.getWallet() + 50);
                    player.setExperience(player.getExperience() + 100);
                    clientHandler.BW.write(
                            "You have completed the quest 'Survive a combat encounter'. You have been rewarded 50 credits and 100 experience");
                    clientHandler.BW.newLine();
                    clientHandler.BW.flush();
                    player.clearQuest();
                }

            }
            // if player is dead, end combat, send the user to the spawn point, and half
            // their wallet
            if (player.getHealth() <= 0) {
                clientHandler.BW.write(
                        "You're blacking out! You feel a strange force pulling you back to a safe location...");
                clientHandler.BW.newLine();
                clientHandler.BW.flush();
                combatActive = false;
                playerWin = false;
                player.setHealth(player.getMaxHealth());
                player.setWallet(player.getWallet() / 2);
                player.setPlayerLocationReference(player.getPlayerSpawnPoint());
                break;
            }
        }
        return combatActive;
    }
}
