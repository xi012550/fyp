package Combat;

import Entities.Enemy;
import Entities.Player;

public class Combat {

    // create a basic attack commands that'll be used for player vs enemy combat.
    // take in the player and enemy objects and modify their values after the combat
    // is over.

    public static void basicPlayerAttack(Player player, Enemy enemy) {
        // check if the attack will miss by taking enemy evasion as the chance of the
        // move hitting
        if (Math.random() > enemy.getEvasion()) {
            // calculate damage and round it to the nearest whole number
            int damage = (int) Math.round(player.getDamage() * enemy.getDefence());
            // modify enemy health
            enemy.setHealth(enemy.getHealth() - damage);
            System.out.println("The attack hit!");
        } else {
            System.out.println("The attack missed!");
        }
    }

    public static void basicEnemyAttack(Enemy enemy, Player player) {
        // check if the attack will miss by taking player evasion as the chance of the
        // move hitting
        if (Math.random() > player.getEvasion()) {
            // calculate damage and round it to the nearest whole number
            int damage = (int) Math.round(enemy.getDamage() * player.getDefence());
            // modify player health
            player.setHealth(player.getHealth() - damage);
            System.out.println("The attack hit!");
        } else {
            System.out.println("The attack missed!");
        }
    }

}
