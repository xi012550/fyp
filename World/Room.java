package World;

import Entities.Enemy;
import Entities.NPC;
import Entities.NPCMerchant;
import Entities.Player;
import Objects.Items;

import java.util.ArrayList;

public class Room implements java.io.Serializable {

    public String description; // The description of the room.
    public int id; // The unique ID of the room.
    Room north, east, south, west, up, down; // The adjacent rooms in the game world.
    public ArrayList<Items> roomItems = new ArrayList<>(); // A list of objects in the room.
    public ArrayList<Enemy> enemies = new ArrayList<>(); // A list of enemies in the room.
    public ArrayList<Player> playerList = new ArrayList<>(); // A list of players in the room.
    public ArrayList<NPC> npcList = new ArrayList<>(); // A list of NPCs in the room.

    // Constructor for the Room class that takes in an ID and description as
    // parameters.
    Room(int id, String description) {
        this.id = id;
        this.description = description;
    }

    // Method to set the location of surrounding.
    public void setLocation(Room north, Room east, Room south, Room west, Room up, Room down) {
        this.north = north;
        this.east = east;
        this.south = south;
        this.west = west;
        this.up = up;
        this.down = down;
    }

    // method to return available exits

    public String getExits() {
        String exits = "Exits: ";
        if (north != null) {
            exits += "north ";
        }
        if (east != null) {
            exits += "east, ";
        }
        if (south != null) {
            exits += "south ";
        }
        if (west != null) {
            exits += "west ";
        }
        if (up != null) {
            exits += "up ";
        }
        if (down != null) {
            exits += "down.";
        }
        return exits;
    }

    // Getter methods for the adjacent rooms.
    public Room getNorth() {
        return north;
    }

    public Room getEast() {
        return east;
    }

    public Room getSouth() {
        return south;
    }

    public Room getWest() {
        return west;
    }

    public Room getUp() {
        return up;
    }

    public Room getDown() {
        return down;
    }

    public ArrayList<Enemy> getEnemies() {
        return enemies;
    }

    public ArrayList<Items> getRoomItems() {
        return roomItems;
    }

    public ArrayList<Player> getPlayers() {
        return playerList;
    }

    public ArrayList<NPC> getNPCs() {
        return npcList;
    }

    // Method to add a player to the room.
    public void addPlayer(Player player) {
        playerList.add(player);
    }

    // Method to remove a player from the room.
    public void removePlayer(Player player) {
        playerList.remove(player);
    }

    // Method to remove an object from the room.
    public void removeObject(Items item) {
        roomItems.remove(item);
    }

    // Method to add an object to the room.
    public void addObject(Items item) {
        roomItems.add(item);
    }

    // Method to add an enemy to the room.
    public void addEnemy(Enemy enemy) {
        enemies.add(enemy);
    }

    // Method to remove an enemy from the room.
    public void removeEnemy(Enemy enemy) {
        enemies.remove(enemy);
    }

    // Method to get the description of the room, including any objects in it.
    public String getDescription() {
        StringBuilder sb = new StringBuilder();
        sb.append(description);
        for (Items item : roomItems) {
            sb.append(item.getName());
            sb.append("\n");
        }

        for (Enemy enemy : enemies) {
            sb.append(enemy.getEnemyName());
            sb.append("\n");
        }

        for (NPC npc : npcList) {
            sb.append(npc.getName());
            sb.append("\n");
        }

        for (Player player : playerList) {
            sb.append(player.getPlayerName());
            sb.append("\n");
        }

        return sb.toString();
    }

    // Getter method for the list of items in the room.
    public ArrayList<Items> getItems() {
        return roomItems;
    }

    // create a "has player" function
    public boolean hasPlayer() {
        return !playerList.isEmpty();
    }

    public Object getDungeonID() {
        return null;
    }

    public void refresh() {
    }

    public void addNPC(NPC npc) {
        npcList.add(npc);
    }

}
