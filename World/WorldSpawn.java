package World;

import java.util.ArrayList;
import java.util.LinkedList;

import Entities.Enemy;
import Entities.NPCMerchant;
import Entities.NPCQuest;
import Entities.NPCTrainer;
import Objects.Items;
import Objects.Machete;
import Objects.Pistol;
import World.Room;

public class WorldSpawn {

    public static final Room[] worldList = new Room[0];

    public static ArrayList<Items> addBasicMachete() {
        Machete rustyMachete = new Machete(1);
        ArrayList<Items> items = new ArrayList<Items>();
        items.add(rustyMachete);

        return items;
    }

    public static ArrayList<Items> addMachete() {
        Machete machete = new Machete(2);
        ArrayList<Items> items = new ArrayList<Items>();
        items.add(machete);

        return items;
    }

    public static ArrayList<Items> addAdvancedMachete() {
        Machete machete = new Machete(3);
        ArrayList<Items> items = new ArrayList<Items>();
        items.add(machete);

        return items;
    }

    public static ArrayList<Items> addBasicPistol() {
        Pistol damagedPistol = new Pistol(1);
        ArrayList<Items> items = new ArrayList<Items>();
        items.add(damagedPistol);

        return items;
    }

    public static ArrayList<Items> addPistol() {
        Pistol pistol = new Pistol(2);
        ArrayList<Items> items = new ArrayList<Items>();
        items.add(pistol);

        return items;
    }

    public static ArrayList<Items> addAdvancedPistol() {
        Pistol pistol = new Pistol(3);
        ArrayList<Items> items = new ArrayList<Items>();
        items.add(pistol);

        return items;
    }

    // add a level 2 raider in the same way addweakraiderpair does by returning a
    // list of enemies
    public static ArrayList<Enemy> addRaider() {
        // return a list of one level 1 raider
        ArrayList<Enemy> enemies = new ArrayList<Enemy>();
        enemies.add(new Entities.enemyRaider(1, 0));

        return enemies;
    }

    public static ArrayList<Enemy> addRaiderPair() {
        // return a list of two weak raiders
        ArrayList<Enemy> enemies = new ArrayList<Enemy>();
        enemies.add(new Entities.enemyRaider(1, 0));
        enemies.add(new Entities.enemyRaider(1, 1));

        return enemies;
    }

    // create a method that takes in a list of enemies and adds them to the room
    public static void addEnemies(Room room, Entities.Enemy[] enemies) {
        for (Entities.Enemy enemy : enemies) {
            room.addEnemy(enemy);
        }
    }

    // create a method that adds a merchant to the room
    public static void addMerchant(Room room) {
        // create an arraylist of items that the merchant has for sale
        ArrayList<Items> itemsForSale = new ArrayList<Items>();
        // add all 3 machetes to the arraylist
        Machete rustyMachete = new Machete(1);
        Machete machete = new Machete(2);
        Machete sharpMachete = new Machete(3);
        itemsForSale.add(rustyMachete);
        itemsForSale.add(machete);
        itemsForSale.add(sharpMachete);

        NPCMerchant startingMerchant = new NPCMerchant("Masked Merchant", "I have some items for sale", itemsForSale);
        room.addNPC(startingMerchant);

        NPCTrainer startingTrainer = new NPCTrainer("Masked Trainer",
                "I can train you for the wasteland if you have the money");
        room.addNPC(startingTrainer);

        NPCQuest startingQuest = new NPCQuest("Masked Quest Giver", "I have a quest for you, if you are interested");
        room.addNPC(startingQuest);
    }

    public static LinkedList<Room> createRooms() {

        LinkedList<Room> worldList = new LinkedList<Room>();

        // make the lists empty

        // Create starting location
        Room spawnPoint = new Room(1, "This is the spawnpoint" + "\n");
        addMerchant(spawnPoint);
        worldList.add(spawnPoint);
        spawnPoint.addObject(new Objects.Pistol(1));
        spawnPoint.addObject(new Objects.Machete(2));
        spawnPoint.addObject(new Objects.Machete(1));
        spawnPoint.addEnemy(new Entities.enemyRaider(1, 0));
        Room spawnN = new Room(2, "This is the room north of where you spawn" + "\n");
        worldList.add(spawnN);
        Room spawnS = new Room(3, "This is the room south of where you spawn" + "\n");
        worldList.add(spawnS);
        Room spawnE = new Room(4, "This is the room east of where you spawn" + "\n");
        worldList.add(spawnE);
        Room spawnW = new Room(5, "This is the room west of where you spawn" + "\n");
        worldList.add(spawnW);
        Room spawnSS = new Room(6, "This is the room south of the room south of where you spawn" + "\n");
        worldList.add(spawnSS);
        Room spawnNN = new Room(7, "This is the room north of the room north of where you spawn" + "\n");
        worldList.add(spawnNN);
        Room spawnSSD = new Room(8, "This is the room down from the room south of where you spawn" + "\n");
        worldList.add(spawnSSD);
        Room spawnNNU = new Room(9, "This is the room up from the room north of where you spawn" + "\n");
        worldList.add(spawnNNU);

        // Create tutorial dungeon
        ArrayList<ArrayList<Enemy>> tutorialStartEnemies = new ArrayList<>();
        ArrayList<ArrayList<Items>> tutorialStartLoot = new ArrayList<>();
        Dungeon tutorialStart = new Dungeon(10, 0,
                "Welcome to the tutorial! Feel free to progress through the rooms and follow the instructions to the end"
                        + "\n",
                tutorialStartEnemies, tutorialStartLoot);
        worldList.add(tutorialStart);

        ArrayList<ArrayList<Enemy>> Tutorial1Enemies = new ArrayList<>();
        Tutorial1Enemies.add(addRaider());
        ArrayList<ArrayList<Items>> Tutorial1Loot = new ArrayList<>();
        Tutorial1Loot.add(addBasicPistol());
        Tutorial1Loot.add(addBasicMachete());
        Tutorial1Loot.add(addPistol());
        Tutorial1Loot.add(addMachete());
        Tutorial1Loot.add(addAdvancedPistol());
        Tutorial1Loot.add(addAdvancedMachete());
        Dungeon tutorial1 = new Dungeon(11, 0, "This is the first room of the tutorial dungeon" + "\n",
                Tutorial1Enemies,
                Tutorial1Loot);
        worldList.add(tutorial1);

        ArrayList<ArrayList<Enemy>> Tutorial2Enemies = new ArrayList<>();
        ArrayList<ArrayList<Items>> Tutorial2Loot = new ArrayList<>();
        Dungeon tutorial2 = new Dungeon(12, 0, "This is the second room of the tutorial dungeon" + "\n",
                Tutorial2Enemies, Tutorial2Loot);
        worldList.add(tutorial2);

        ArrayList<ArrayList<Enemy>> Tutorial3Enemies = new ArrayList<>();
        ArrayList<ArrayList<Items>> Tutorial3Loot = new ArrayList<>();
        Dungeon tutorial3 = new Dungeon(13, 0, "This is the third room of the tutorial dungeon" + "\n",
                Tutorial3Enemies,
                Tutorial3Loot);
        worldList.add(tutorial3);

        ArrayList<ArrayList<Enemy>> Tutorial4Enemies = new ArrayList<>();
        ArrayList<ArrayList<Items>> Tutorial4Loot = new ArrayList<>();
        Dungeon tutorial4 = new Dungeon(14, 0, "This is the fourth room of the tutorial dungeon" + "\n",
                Tutorial4Enemies, Tutorial4Loot);
        worldList.add(tutorial4);

        ArrayList<ArrayList<Enemy>> Tutorial5Enemies = new ArrayList<>();
        ArrayList<ArrayList<Items>> Tutorial5Loot = new ArrayList<>();
        Dungeon tutorial5 = new Dungeon(15, 0, "This is the fifth room of the tutorial dungeon" + "\n",
                Tutorial5Enemies,
                Tutorial5Loot);
        worldList.add(tutorial5);

        // Set the connections between the rooms
        spawnPoint.setLocation(spawnN, null, spawnS, null, null, null);
        spawnS.setLocation(spawnPoint, null, spawnSS, null, null, null);
        spawnN.setLocation(spawnNN, null, spawnPoint, null, null, null);
        spawnE.setLocation(null, null, null, spawnPoint, null, null);
        spawnW.setLocation(null, spawnPoint, null, null, null, null);
        spawnSS.setLocation(spawnS, null, null, null, null, spawnSSD);
        spawnNN.setLocation(null, null, spawnN, null, spawnNNU, null);
        spawnSSD.setLocation(null, null, null, null, spawnSS, tutorialStart);
        spawnNNU.setLocation(null, null, null, null, null, spawnNN);

        tutorialStart.setLocation(tutorial1, null, null, null, spawnSSD, null);
        tutorial1.setLocation(tutorial2, null, tutorialStart, null, null, null);
        tutorial2.setLocation(tutorial3, null, tutorial1, null, null, null);
        tutorial3.setLocation(tutorial4, null, tutorial2, null, null, null);
        tutorial4.setLocation(tutorial5, null, tutorial3, null, null, null);
        tutorial5.setLocation(null, null, tutorial4, null, null, null);

        System.out.println("World Initialised" + "\n");

        return worldList;
    }

}
