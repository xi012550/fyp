package World;

import Objects.Machete;

import java.util.Arrays;
import java.util.LinkedList;

public class Map implements java.io.Serializable {

    public LinkedList<Room> worldList = new LinkedList<Room>(); // A list of all the rooms in the game world.

    // Default constructor for the Map class.
    public Map() {
    }

    // Method to add a room to the game world.
    public void addRoom(Room room) {
        worldList.add(room);
    }

    // Method to initialise the game world with rooms and objects.
    public void initialiseWorld() {

        // create the spawn point using worldspawn then add the rooms to teh world
        worldList = WorldSpawn.createRooms();

    }

    // Method to print out a confirmation that the game world exists.
    public void returnRoomConfirmation() {
        System.out.print("This world does in fact exist, as proof allow me to do this");
        for (Room room : worldList) {
            System.out.print(room.description);
        }
    }
}
