package World;

import java.util.ArrayList;
import java.util.Random;

import Entities.Enemy;
import Objects.Items;

public class Dungeon extends Room implements java.io.Serializable {

    private ArrayList<ArrayList<Enemy>> enemyList = new ArrayList<>();
    private ArrayList<ArrayList<Items>> lootList = new ArrayList<>();
    private int dungeonID;

    public Dungeon(int id, int dungeonID, String description, ArrayList<ArrayList<Enemy>> tempDungeonEnemies,
            ArrayList<ArrayList<Items>> lootList) {
        super(id, description);
        this.enemyList = tempDungeonEnemies;
        this.lootList = lootList;
        this.dungeonID = dungeonID;
    }

    public void refresh() {
        // Remove current enemies and loot.
        enemies.clear();
        roomItems.clear();

        Random rand = new Random();

        if (!enemyList.isEmpty()) {
            ArrayList<Enemy> randomEnemy = enemyList.get(rand.nextInt(enemyList.size()));
            addEnemy(randomEnemy);
        }

        // if the lootlist isnt empty, add a random piece of loot from the arraylist
        if (!lootList.isEmpty()) {
            ArrayList<Items> randomLoot = lootList.get(rand.nextInt(lootList.size()));
            addItem(randomLoot);
        }

    }

    // override the addenemy method to add all enemies in the arraylist
    public void addEnemy(ArrayList<Enemy> enemy) {
        for (Enemy e : enemy) {
            enemies.add(e);
        }
    }

    // override the additem method to add all items in the arraylist
    public void addItem(ArrayList<Items> items) {
        for (Items i : items) {
            roomItems.add(i);
        }
    }

    public Object getDungeonID() {
        return dungeonID;
    }

}