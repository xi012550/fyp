package Quests;

public class Quest implements java.io.Serializable {

    private int ID;
    private String description;
    private boolean isComplete;

    public Quest(int ID, String description) {
        this.ID = ID;
        this.description = description;
        this.isComplete = false;
    }

    public void complete() {
        this.isComplete = true;
    }

    public boolean isComplete() {
        return this.isComplete;
    }

    public int getID() {
        return this.ID;
    }

    public int setID() {
        return this.ID;
    }

    public String getDescription() {
        return this.description;
    }

    public String toString() {
        return "Quest Complete!" + "\n";
    }

}
